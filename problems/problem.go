package problems

import (
	"encoding/json"

	"github.com/gin-gonic/gin"
)

type Problem interface {
	error
	StatusCode() int
	Title() string
	Detail() string
	AsMap() gin.H
	MarshalJSON() ([]byte, error)
	ViewName() string
	ViewData() gin.H
}

type BaseProblem struct {
	Problem
	status   int
	title    string
	detail   string
	problems gin.H
	viewName string
	viewData gin.H
}

func (problem *BaseProblem) Title() string {
	return problem.title
}

func (problem *BaseProblem) Error() string {
	return problem.detail
}

func (problem *BaseProblem) StatusCode() int {
	return problem.status
}

func (problem *BaseProblem) Detail() string {
	return problem.detail
}

func (problem *BaseProblem) ViewName() string {
	return problem.viewName
}

func (problem *BaseProblem) ViewData() gin.H {
	return problem.viewData
}

func (problem *BaseProblem) AsMap() gin.H {
	h := gin.H{
		"status": problem.status,
		"title":  problem.title,
	}

	if problem.detail != "" {
		h["detail"] = problem.detail
	}

	if problem.problems != nil {
		h["problems"] = problem.problems
	}

	return h
}

func (problem *BaseProblem) MarshalJSON() ([]byte, error) {
	h := problem.AsMap()
	return json.Marshal(h)
}
