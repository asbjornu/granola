package problems

import "net/http"

type UnauthorizedProblem struct {
	BaseProblem
}

func Unauthorized(detail string) *UnauthorizedProblem {
	return &UnauthorizedProblem{
		BaseProblem{
			status: http.StatusUnauthorized,
			title:  "Unauthorized",
			detail: detail,
		},
	}
}
