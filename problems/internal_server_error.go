package problems

import "net/http"

type InternalServerErrorProblem struct {
	BaseProblem
}

func InternalServerError(detail string) *InternalServerErrorProblem {
	return &InternalServerErrorProblem{
		BaseProblem{
			status: http.StatusInternalServerError,
			title:  "Internal Server Error",
			detail: detail,
		},
	}
}
