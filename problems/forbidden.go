package problems

import "net/http"

type ForbiddenProblem struct {
	BaseProblem
}

func Forbidden(detail string) *ForbiddenProblem {
	return &ForbiddenProblem{
		BaseProblem{
			status: http.StatusForbidden,
			title:  "Forbidden",
			detail: detail,
		},
	}
}
