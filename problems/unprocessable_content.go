package problems

import (
	"net/http"

	"github.com/gin-gonic/gin"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
)

type UnprocessableContentProblem struct {
	BaseProblem
}

func UnprocessableContent(err error, translator ut.Translator, viewName string, viewData gin.H, problems gin.H) *UnprocessableContentProblem {
	detail := err.Error()

	if validationErrors, ok := err.(validator.ValidationErrors); ok && len(validationErrors) > 0 {
		if len(validationErrors) == 1 {
			detail = validationErrors[0].Translate(translator)
		} else {
			if problems == nil {
				problems = make(gin.H)
			}

			for _, validationError := range validationErrors {
				field := validationError.Field()
				if _, ok := problems[field]; !ok {
					problems[field] = validationError.Translate(translator)
				}
			}
		}
	}

	return &UnprocessableContentProblem{
		BaseProblem{
			status:   http.StatusUnprocessableEntity,
			title:    "Unprocessable Entity",
			detail:   detail,
			problems: problems,
			viewName: viewName,
			viewData: viewData,
		},
	}
}
