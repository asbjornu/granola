package problems

import "net/http"

type NotFoundProblem struct {
	BaseProblem
}

func NotFound(detail string) *NotFoundProblem {
	return &NotFoundProblem{
		BaseProblem{
			status: http.StatusNotFound,
			title:  "Not Found",
			detail: detail,
		},
	}
}
