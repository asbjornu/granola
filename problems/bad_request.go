package problems

import "net/http"

type BadRequestProblem struct {
	BaseProblem
}

func BadRequest(detail string) *BadRequestProblem {
	return &BadRequestProblem{
		BaseProblem{
			status: http.StatusBadRequest,
			title:  "Bad Request",
			detail: detail,
		},
	}
}
