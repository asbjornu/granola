package main

import (
	"fmt"
	"granola/models"
	"net/http"
	"sync"
	"sync/atomic"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestRegisterValidation(t *testing.T) {
	r := NewTestRouterUser(t)

	registerUser := func(r *TestRouter, email string, handle string, invite string, password string) TestResponse {
		obj := gin.H{
			"email":    email,
			"handle":   handle,
			"invite":   invite,
			"password": password,
		}
		return r.requestJson("POST", "/register", obj, nil)
	}

	t.Run("user can't register with empty e-mail", func(t *testing.T) {
		response := registerUser(&r, "", "test-user", "test-invite", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
		assert.Equal(t, "Email is a required field", json["detail"])
	})

	t.Run("user can't register with empty handle", func(t *testing.T) {
		response := registerUser(&r, "test-user@example.com", "", "test-invite", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
		assert.Equal(t, "Handle is a required field", json["detail"])
	})

	t.Run("user can't register with empty invite key", func(t *testing.T) {
		response := registerUser(&r, "test-user@example.com", "test-user", "", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
		assert.Equal(t, "Invite is a required field", json["detail"])
	})

	t.Run("user can't register with empty password", func(t *testing.T) {
		response := registerUser(&r, "test-user@example.com", "test-user", "test-invite", "")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
		assert.Equal(t, "Password is a required field", json["detail"])
	})

	t.Run("user can't register with too short e-mail", func(t *testing.T) {
		response := registerUser(&r, "u", "test-user", "test-invite", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
		assert.Equal(t, "Invalid e-mail address", json["detail"])
	})

	t.Run("user can't register with invalid e-mail", func(t *testing.T) {
		response := registerUser(&r, "abc", "test-user", "test-invite", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
		assert.Equal(t, "Invalid e-mail address", json["detail"])
	})

	t.Run("user can't register with too short password", func(t *testing.T) {
		response := registerUser(&r, "test-user@example.com", "test-user", "test-invite", "p")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
		assert.Equal(t, "Password must be at least 5 characters long", json["detail"])
	})

	t.Run("user can't register with invalid invitation with JSON", func(t *testing.T) {
		response := registerUser(&r, "test-user@example.com", "test-user", "invalid-invitation", "test-password")
		assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
		assert.Equal(t, "Invalid invitation", response.BodyJSON()["detail"])
	})

	t.Run("user can't register with invalid invitation with HTML", func(t *testing.T) {
		response := r.requestHTML("POST", "/register", gin.H{
			"email":    "test-user@example.com",
			"handle":   "test-user",
			"invite":   "invalid-invitation",
			"password": "test-password",
		}, nil)
		assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
		assert.Contains(t, response.BodyString(), "Invalid invitation")
	})
}

func TestRegister(t *testing.T) {
	r := NewTestRouterUser(t)

	r.createInvite("test-invite", false)
	r.createInvite("test-invite-2", false)

	t.Run("user 1 can register with invite key 1", func(t *testing.T) {
		response := r.requestJson("POST", "/register", gin.H{
			"email":    "test-user@example.com",
			"handle":   "test-user",
			"invite":   "test-invite",
			"password": "test-password",
		}, nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/users/81855ad8-681d-4d86-91e9-1e00167939cb", json["id"])
		assert.Equal(t, "test-user@example.com", json["email"])
		assert.Equal(t, "test-user", json["handle"])
	})

	t.Run("can't register two users with same email address with JSON", func(t *testing.T) {
		response := r.requestJson("POST", "/register", gin.H{
			"email":    "test-user@example.com",
			"handle":   "test-user",
			"invite":   "test-invite-2",
			"password": "test-password",
		}, nil)
		assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
		assert.Contains(t, response.BodyJSON()["detail"], "users.email")
	})

	t.Run("can't register two users with same email address with HTML", func(t *testing.T) {
		response := r.requestHTML("POST", "/register", gin.H{
			"email":    "test-user@example.com",
			"handle":   "test-user",
			"invite":   "test-invite-2",
			"password": "test-password",
		}, nil)
		assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
		assert.Contains(t, response.BodyString(), "Email address already in use")
	})

	t.Run("Second user can register with second invite key", func(t *testing.T) {
		response := r.requestJson("POST", "/register", gin.H{
			"email":    "test-user-2@example.com",
			"handle":   "test-user2",
			"invite":   "test-invite-2",
			"password": "test-password",
		}, nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/users/95af5a25-3679-41ba-a2ff-6cd471c483f1", json["id"])
		assert.Equal(t, "test-user-2@example.com", json["email"])
		assert.Equal(t, "test-user2", json["handle"])
	})
}

func TestRegisterSpam(t *testing.T) {
	r := NewTestRouterUser(t)
	r.createInvite("test-invite", false)

	N := 1000

	if testing.Short() {
		N = 100
	}

	t.Run("registering the same user multiple times should fail", func(t *testing.T) {
		var wg sync.WaitGroup
		var succeeded uint64

		wg.Add(N)
		for i := 0; i < N; i++ {
			i := i
			go func() {
				defer wg.Done()

				email := fmt.Sprintf("user-%d@example.com", i)
				response := r.requestJson("POST", "/register", gin.H{
					"email":    email,
					"handle":   "test-user",
					"invite":   "test-invite",
					"password": "test-password",
				}, nil)

				if response.StatusCode == http.StatusCreated {
					atomic.AddUint64(&succeeded, 1)
				} else {
					json := response.BodyJSON()
					assert.Equal(t, http.StatusUnprocessableEntity, response.StatusCode)
					assert.Equal(t, "Invalid invitation", json["detail"])
				}
			}()
		}
		wg.Wait()

		assert.Equal(t, uint64(1), succeeded)
	})
}

func TestLogin(t *testing.T) {
	r := NewTestRouterUser(t)

	t.Run("user can't login with non-existing credentials", func(t *testing.T) {
		response := r.requestJson("POST", "/login", gin.H{
			"email":    "test-user@example.com",
			"password": "test-password",
		}, nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
		assert.Equal(t, "Invalid credentials", json["detail"])
	})

	r.createInvite("test-invite", false)
	r.createUser("test-user@example.com", "test-user", "test-password", "test-invite")

	t.Run("user can't login with invalid password", func(t *testing.T) {
		response := r.requestJson("POST", "/login", gin.H{
			"email":    "test-user@example.com",
			"password": "invalid-password",
		}, nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
		assert.Equal(t, "Invalid credentials", json["detail"])
	})

	var cookies []string
	t.Run("user can login with valid credentials", func(t *testing.T) {
		response := r.requestJson("POST", "/login", gin.H{
			"email":    "test-user@example.com",
			"password": "test-password",
		}, nil)
		cookies = response.Cookies
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "Successfully authenticated user", json["detail"])
	})

	t.Run("home page welcomes the authenticated user", func(t *testing.T) {
		response := r.requestHTML("GET", "/", nil, cookies)
		assert.Contains(t, response.BodyString(), "test-user's user profile")
	})

	t.Run("user can log out", func(t *testing.T) {
		response := r.requestHTML("POST", "/logout", nil, cookies)
		cookies = response.Cookies
		assert.Equal(t, http.StatusFound, response.StatusCode)
		assert.Equal(t, "/", response.Location)
	})

	t.Run("home page no longer welcomes the user", func(t *testing.T) {
		response := r.requestHTML("GET", "/", nil, cookies)
		assert.NotContains(t, response.BodyString(), `Logged in as "test-user@example.com"`)
		assert.Contains(t, response.BodyString(), "see yourself inside")
	})
}

func TestUserEditProfile(t *testing.T) {
	r := NewTestRouterUser(t)
	r.createTestUser()
	cookies := r.loginUser("test-user@example.com", "test-password")

	t.Run("user can GET their own profile", func(t *testing.T) {
		response := r.requestJson("GET", "/profile", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
	})

	t.Run("user can PATCH their own profile with new e-mail", func(t *testing.T) {
		response := r.requestJson("PATCH", "/profile", gin.H{"email": "test-user2@example.com"}, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "test-user2@example.com", json["email"])
		assert.Equal(t, "test-user", json["handle"]) // handle shouldn't change
	})

	t.Run("user can PATCH their own profile with new handle", func(t *testing.T) {
		response := r.requestJson("PATCH", "/profile", gin.H{"handle": "test-user2"}, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "test-user2", json["handle"])
		assert.Equal(t, "test-user2@example.com", json["email"]) // email shouldn't change
	})

	t.Run("user can PATCH their own profile with new password", func(t *testing.T) {
		response := r.requestJson("PATCH", "/profile", gin.H{"old-password": "test-password", "password": "test-password2"}, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
	})

	t.Run("user can't PATCH their own profile with invalid email", func(t *testing.T) {
		response := r.requestJson("PATCH", "/profile", gin.H{"email": "test-user2-at-example.com"}, cookies)
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	})

	t.Run("user can log out", func(t *testing.T) {
		response := r.requestHTML("POST", "/logout", nil, cookies)
		assert.Equal(t, http.StatusFound, response.StatusCode)
		assert.Equal(t, "/", response.Location)
	})

	t.Run("user can login with new e-mail and password", func(t *testing.T) {
		cookies := r.loginUser("test-user2@example.com", "test-password2")
		assert.Len(t, cookies, 1)
	})

	t.Run("home page should show the new handle", func(t *testing.T) {
		response := r.requestHTML("GET", "/", nil, cookies)
		assert.Contains(t, response.BodyString(), "test-user2's user profile")
	})
}

func TestInviteForwarding(t *testing.T) {
	r := NewTestRouterUser(t)

	t.Run("GET /register should redirect to /register/", func(t *testing.T) {
		response := r.requestHTML("GET", "/register/", nil, nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, "/register", response.Location)
	})

	t.Run("GET /register with invite key should show the registration form with invite key", func(t *testing.T) {
		response := r.requestHTML("GET", "/register?invite=test-invite", nil, nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), `value="test-invite"`)
	})
}

func TestUserCreateEntry(t *testing.T) {
	r := NewTestRouterUser(t)
	user := r.createTestUser()
	cookies := r.loginUser("test-user@example.com", "test-password")

	t.Run("user can GET empty list of entries without compos", func(t *testing.T) {
		response := r.requestJson("GET", "/entries", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
	})

	openCompo := r.createTestCompo()
	lockedCompo := r.createCompo("test compo 2", true, true, models.CompoTypeUndefined, "")

	t.Run("user can GET empty list of entries", func(t *testing.T) {
		response := r.requestJson("GET", "/entries", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("user can't POST entry for compo ID 0", func(t *testing.T) {
		obj := gin.H{
			"compo":  0,
			"title":  "test-entry",
			"author": "test-author",
			"notes":  "",
		}
		response := r.requestJson("POST", "/entries", obj, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "CompoID is a required field", json["detail"])
	})

	expectedID := "/entries/81855ad8-681d-4d86-91e9-1e00167939cb"
	expectedPreview := fmt.Sprintf("%v/preview", expectedID)
	t.Run("user can POST valid entry", func(t *testing.T) {
		obj := gin.H{
			"compo":  openCompo.ID,
			"title":  "entry 1",
			"author": "author 1",
			"notes":  "**be strong**",
		}
		response := r.requestJson("POST", "/entries", obj, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "author 1", json["author"])
		assert.Equal(t, openCompo.Path(), json["compo"])
		assert.Equal(t, expectedID, json["id"])
		assert.Equal(t, "entry 1", json["title"])
		assert.Equal(t, "**be strong**", json["notes"])
		assert.Equal(t, user.Path(), json["user"])
		assert.Equal(t, false, json["locked"])
		assert.Equal(t, map[string]any{"id": expectedPreview}, json["preview"])
	})

	t.Run("user can GET list of entries", func(t *testing.T) {
		response := r.requestJson("GET", "/entries", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.NotEmpty(t, response.Body)
	})

	t.Run("user can GET their own entry", func(t *testing.T) {
		response := r.requestJson("GET", expectedID, nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "author 1", json["author"])
		assert.Equal(t, openCompo.Path(), json["compo"])
		assert.Equal(t, expectedID, json["id"])
		assert.Equal(t, "entry 1", json["title"])
		assert.Equal(t, "**be strong**", json["notes"])
		assert.Equal(t, user.Path(), json["user"])
		assert.Equal(t, false, json["locked"])
		assert.Equal(t, map[string]any{"id": expectedPreview}, json["preview"])
	})

	t.Run("user can't POST entry for non-existent compo", func(t *testing.T) {
		obj := gin.H{
			"compo":  lockedCompo.ID,
			"title":  "entry 2",
			"author": "author 2",
			"notes":  "**be strong**",
		}
		response := r.requestJson("POST", "/entries", obj, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "compo 2 not found", json["detail"])
	})
}

func TestUserUpdateEntry(t *testing.T) {
	r := NewTestRouterUser(t)
	compo := r.createTestCompo()
	user := r.createTestUser()
	cookies := r.loginUser("test-user@example.com", "test-password")

	entry := r.createUserTestEntry(compo, user)

	t.Run("user can PUT entry", func(t *testing.T) {
		obj := gin.H{
			"title":    "updated entry 1",
			"author":   "updated author 1",
			"platform": "platform",
			"notes":    "notes",
		}
		response := r.requestJson("PUT", entry.Path(), obj, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "updated author 1", json["author"])
		assert.Equal(t, compo.Path(), json["compo"])
		assert.Equal(t, entry.Path(), json["id"])
		assert.Equal(t, "updated entry 1", json["title"])
		assert.Equal(t, "notes", json["notes"])
		assert.Equal(t, user.Path(), json["user"])
		assert.Equal(t, false, json["locked"])
		assert.Equal(t, map[string]any{"id": entry.PreviewPath()}, json["preview"])
	})
}

func TestUserDeleteEntry(t *testing.T) {
	r := NewTestRouterUser(t)

	compo := r.createTestCompo()
	user := r.createTestUser()
	cookies := r.loginUser("test-user@example.com", "test-password")
	entry := r.createUserTestEntry(compo, user)

	t.Run("user can GET empty entry list", func(t *testing.T) {
		response := r.requestJson("GET", "/entries", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.NotEmpty(t, response.BodyString())
	})

	t.Run("user can DELETE their own entry", func(t *testing.T) {
		response := r.requestJson("DELETE", entry.Path(), nil, cookies)
		assert.Equal(t, http.StatusNoContent, response.StatusCode)
		assert.Empty(t, response.Body)
	})

	t.Run("user can GET empty entry list", func(t *testing.T) {
		response := r.requestJson("GET", "/entries", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("user can't DELETE the same entry twice", func(t *testing.T) {
		response := r.requestJson("DELETE", entry.Path(), nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "Not Found", json["title"])
	})
}

func TestUserEntryPreview(t *testing.T) {
	r := NewTestRouterUser(t)

	compo := r.createTestCompo()
	user := r.createTestUser()
	cookies := r.loginUser("test-user@example.com", "test-password")

	entry := r.createUserEntry("test-entry", "test-author", "test-platform", "**be strong**", compo, user)

	t.Run("user can GET entry preview", func(t *testing.T) {
		response := r.requestHTML("GET", entry.PreviewPath(), nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), "#??</h1>")
		assert.Contains(t, response.BodyString(), "test-entry</h1>")
		assert.Contains(t, response.BodyString(), "<h2>By test-author</h2>")
		assert.Contains(t, response.BodyString(), `<p><strong>be strong</strong></p>`)
	})
}

func TestUserEntryPreviewNoMarkdown(t *testing.T) {
	cfg := DefaultTestConfig()
	cfg.Routing.UseEntryMarkdown = false
	r := NewTestRouterUserWithConfig(t, &cfg)

	compo := r.createTestCompo()
	user := r.createTestUser()
	cookies := r.loginUser("test-user@example.com", "test-password")

	notes := "<p>**no markdown or html**</p>"
	entry := r.createUserEntry("test-entry", "test-author", "test-platform", notes, compo, user)

	t.Run("user can GET entry preview", func(t *testing.T) {
		response := r.requestHTML("GET", entry.PreviewPath(), nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), `&lt;p&gt;**no markdown or html**&lt;/p&gt;`)
	})
}

func TestUserCreateUpload(t *testing.T) {
	r := NewTestRouterUser(t)

	compo := r.createTestCompo()
	user := r.createTestUser()
	cookies := r.loginUser("test-user@example.com", "test-password")
	entry := r.createUserTestEntry(compo, user)

	expectedUploadID := fmt.Sprintf("%v/uploads/6694d2c4-22ac-4208-a007-2939487f6999", entry.Path())

	t.Run("user can POST upload", func(t *testing.T) {
		response := r.requestData("POST", fmt.Sprintf("%v/uploads", entry.Path()), []byte{1, 2, 3}, "file.bin", cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "application/octet-stream", json["contentType"])
		assert.Equal(t, entry.Path(), json["entry"])
		assert.Equal(t, "file.bin", json["filename"])
		assert.Equal(t, expectedUploadID, json["id"])
	})

	t.Run("user can GET the created upload", func(t *testing.T) {
		response := r.requestJson("GET", expectedUploadID, nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "application/octet-stream", json["contentType"])
		assert.Equal(t, entry.Path(), json["entry"])
		assert.Equal(t, "file.bin", json["filename"])
		assert.Equal(t, expectedUploadID, json["id"])
	})

	t.Run("user can GET the data of the created upload", func(t *testing.T) {
		response := r.requestHTML("GET", fmt.Sprintf("%v/data", expectedUploadID), nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, []byte("\x01\x02\x03"), response.Body)
	})

	t.Run("GET entry with trailing slash should redirect", func(t *testing.T) {
		response := r.requestHTML("GET", fmt.Sprintf("%v/", entry.Path()), nil, nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, entry.Path(), response.Location)
	})

	t.Run("GET upload with trailing slash should redirect", func(t *testing.T) {
		response := r.requestHTML("GET", fmt.Sprintf("%v/", expectedUploadID), nil, nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, expectedUploadID, response.Location)
	})

	t.Run("GET upload data with trailing slash should redirect", func(t *testing.T) {
		response := r.requestHTML("GET", fmt.Sprintf("%v/data/", expectedUploadID), nil, nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, fmt.Sprintf("%v/data", expectedUploadID), response.Location)
	})

	t.Run("user can't POST upload to a locked entry", func(t *testing.T) {
		entry2 := r.createUserTestEntry(compo, user)
		r.lockEntry(entry2)

		response := r.requestData("POST", fmt.Sprintf("%v/uploads", entry2.Path()), []byte{1, 2, 3}, "file.bin", cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusForbidden, response.StatusCode)
		assert.Equal(t, "entry has been locked", json["detail"])
	})

	t.Run("user can DELETE their own upload", func(t *testing.T) {
		response := r.requestJson("DELETE", expectedUploadID, nil, cookies)
		assert.Equal(t, http.StatusNoContent, response.StatusCode)
		assert.Empty(t, response.Body)
	})
}

func TestUserCreateUploadPathEncoding(t *testing.T) {

	r := NewTestRouterUser(t)

	compo := r.createTestCompo()
	user := r.createTestUser()
	cookies := r.loginUser("test-user@example.com", "test-password")
	entry := r.createUserTestEntry(compo, user)

	expectedUploadID := fmt.Sprintf("%v/uploads/6694d2c4-22ac-4208-a007-2939487f6999", entry.Path())

	filename := "foo\\bar\\\\test\\\\\\tast:😀"

	t.Run("user can POST upload with strange encoding", func(t *testing.T) {
		response := r.requestData("POST", fmt.Sprintf("%v/uploads", entry.Path()), []byte{1, 2, 3}, filename, cookies)
		json := response.BodyJSON()
		assert.Equal(t, filename, json["filename"])
		assert.Equal(t, expectedUploadID, json["id"])
	})

	t.Run("user can GET the upload", func(t *testing.T) {
		response := r.requestJson("GET", expectedUploadID, nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, filename, json["filename"])
	})

}

func TestUserDeleteUpload(t *testing.T) {
	r := NewTestRouterUser(t)
	compo := r.createTestCompo()
	user := r.createTestUser()
	cookies := r.loginUser("test-user@example.com", "test-password")
	entry := r.createUserTestEntry(compo, user)
	upload := r.createUpload(entry, "file.bin", []byte{1, 2, 3})

	t.Run("user can DELETE their own upload", func(t *testing.T) {
		response := r.requestJson("DELETE", upload.Path(), nil, cookies)
		assert.Equal(t, http.StatusNoContent, response.StatusCode)
		assert.Empty(t, response.Body)
	})

	t.Run("user can't DELETE the same upload twice", func(t *testing.T) {
		response := r.requestJson("DELETE", upload.Path(), nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "Not Found", json["title"])
	})
}

func TestXUserAccess(t *testing.T) {
	r := NewTestRouterUser(t)

	compo := r.createTestCompo()
	r.createInvite("test-invite1", false)
	r.createInvite("test-invite2", false)
	user1 := r.createUser("test-user1@example.com", "test-user", "test-password1", "test-invite1")
	r.createUser("test-user2@example.com", "test-user", "test-password2", "test-invite2")
	cookies := r.loginUser("test-user1@example.com", "test-password1")

	entry1 := r.createUserTestEntry(compo, user1)
	entry1uploadPath := fmt.Sprintf("%v/uploads", entry1.Path())

	t.Run("user A can POST an upload to their own entry", func(t *testing.T) {
		r.requestData("POST", entry1uploadPath, []byte{1, 2, 3}, "file.bin", cookies)
	})

	cookies = r.loginUser("test-user2@example.com", "test-password2")

	t.Run("user B can't GET user A's entry", func(t *testing.T) {
		response := r.requestJson("GET", entry1.Path(), nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "Not Found", json["title"])
	})

	t.Run("user B can't DELETE user A's entry", func(t *testing.T) {
		response := r.requestJson("DELETE", entry1.Path(), nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "Not Found", json["title"])
	})

	t.Run("user B can't GET user A's upload", func(t *testing.T) {
		response := r.requestJson("GET", fmt.Sprintf("%v/1", entry1uploadPath), nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "Not Found", json["title"])
	})

	t.Run("user B can't POST an upload to user A's entry", func(t *testing.T) {
		response := r.requestData("POST", entry1uploadPath, []byte{1, 2, 3}, "file.bin", cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "Not Found", json["title"])
	})

	t.Run("user B can't GET the data of user A's upload", func(t *testing.T) {
		response := r.requestJson("GET", fmt.Sprintf("%v/1/data", entry1uploadPath), nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "Not Found", json["title"])
	})

	t.Run("user B can't DELETE user A's upload", func(t *testing.T) {
		response := r.requestJson("DELETE", fmt.Sprintf("%v/1", entry1uploadPath), nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "Not Found", json["title"])
	})

	t.Run("user B can't PUT to user A's entry", func(t *testing.T) {
		obj := gin.H{
			"title":    "updated entry 1",
			"author":   "updated author 1",
			"platform": "platform",
			"notes":    "notes",
		}
		response := r.requestJson("PUT", entry1.Path(), obj, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "Not Found", json["title"])
	})
}

func TestUserVoting(t *testing.T) {
	r := NewTestRouterUser(t)

	var compos []*models.Compo
	for i := 0; i < 5; i++ {
		compo := r.createCompo(fmt.Sprintf("test compo %d", i), true, false, models.CompoTypeUndefined, "")
		compos = append(compos, compo)
	}

	r.createInvite("test-invite", true)
	user := r.createUser("test-user@example.com", "test-user", "test-password", "test-invite")

	t.Run("inheriting CanVote=true from invite", func(t *testing.T) {
		assert.Equal(t, true, user.Permissions.CanVote)
	})

	cookies := r.loginUser("test-user@example.com", "test-password")

	t.Run("no shown entry should not allow voting", func(t *testing.T) {
		response := r.requestHTML("GET", "/voting/live", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), "There are currently nothing to live-vote on.")
	})

	var entryIDs []uuid.UUID
	t.Run("create playlist entries", func(t *testing.T) {
		for i := 0; i < 10; i++ {
			entry := r.createUserTestEntry(compos[0], user)
			entryIDs = append(entryIDs, entry.ID)
		}
	})

	playlist := r.createPlaylist("test-playlist", entryIDs)

	t.Run("show playlist should allow voting for 1st entry", func(t *testing.T) {
		t.Run("Can't preview non-showed entry", func(t *testing.T) {
			response := r.requestJson("GET", fmt.Sprintf("/playlists/1/entries/%v", entryIDs[0]), nil, cookies)
			json := response.BodyJSON()
			assert.Equal(t, http.StatusNotFound, response.StatusCode)
			assert.Equal(t, "Not Found", json["title"])
		})

		_, err := playlist.Show(r.DB)

		t.Run("Can preview showed entry", func(t *testing.T) {
			response := r.requestHTML("GET", fmt.Sprintf("/playlists/1/entries/%v", entryIDs[0]), nil, cookies)
			assert.Equal(t, http.StatusOK, response.StatusCode)
			assert.Contains(t, response.BodyString(), "#1")
		})

		assert.NoError(t, err)
		assert.Equal(t, 10, len(playlist.Entries))

		response := r.requestHTML("GET", "/voting/live", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), fmt.Sprintf("<iframe src=\"/playlists/1/entries/%v\"></iframe>", entryIDs[0]))

		t.Run("only the compo associated with the playlist should be locked", func(t *testing.T) {
			for i := 0; i < 5; i++ {
				compo := &models.Compo{}
				r.DB.First(&compo, i+1)

				assert.Equal(t, i == 0, compo.Locked)
			}
		})
	})

	t.Run("showing 5 entries should allow voting for the 5th entry", func(t *testing.T) {
		t.Run("Can't preview non-showed entry", func(t *testing.T) {
			response := r.requestJson("GET", fmt.Sprintf("/playlists/1/entries/%v", entryIDs[4]), nil, cookies)
			assert.Equal(t, http.StatusNotFound, response.StatusCode)
		})

		err := playlist.Entries[4].Show(r.DB)
		assert.NoError(t, err)

		t.Run("Can preview showed entry", func(t *testing.T) {
			response := r.requestHTML("GET", fmt.Sprintf("/playlists/1/entries/%v", entryIDs[4]), nil, cookies)
			assert.Equal(t, http.StatusOK, response.StatusCode)
			assert.Contains(t, response.BodyString(), "#5")
		})

		response := r.requestHTML("GET", "/voting/live", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), fmt.Sprintf("<iframe src=\"/playlists/1/entries/%v\"></iframe>", entryIDs[4]))
	})

	t.Run("live voting reversed", func(t *testing.T) {
		response := r.requestJson("GET", "/voting/live", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		entries := response.BodyJSON()["entries"].([]any)
		assert.Equal(t, 2, len(entries))
		assert.Equal(t, fmt.Sprintf("/playlists/1/entries/%v", entryIDs[4]), entries[0])
		assert.Equal(t, fmt.Sprintf("/playlists/1/entries/%v", entryIDs[0]), entries[1])
	})

	t.Run("voting for fifth entry should work", func(t *testing.T) {
		response := r.requestJson("POST", fmt.Sprintf("/playlists/1/entries/%v/votes", entryIDs[4]), gin.H{"score": 3}, cookies)
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, 3.0, response.BodyJSON()["score"])
	})

	t.Run("fifth entry should have a score of 3", func(t *testing.T) {
		response := r.requestHTML("GET", "/voting/live", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), `<li class="star star-3 selected">`)
	})

	t.Run("max score", func(t *testing.T) {
		response := r.requestJson("POST", fmt.Sprintf("/playlists/1/entries/%v/votes", entryIDs[4]), gin.H{"score": 5}, cookies)
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, 5.0, response.BodyJSON()["score"])
	})

	t.Run("too small score", func(t *testing.T) {
		response := r.requestJson("POST", fmt.Sprintf("/playlists/1/entries/%v/votes", entryIDs[4]), gin.H{"score": -1}, cookies)
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	})

	t.Run("too large score", func(t *testing.T) {
		response := r.requestJson("POST", fmt.Sprintf("/playlists/1/entries/%v/votes", entryIDs[4]), gin.H{"score": 6}, cookies)
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	})

	t.Run("fifth entry should have a score of 5", func(t *testing.T) {
		response := r.requestHTML("GET", "/voting/live", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), `<li class="star star-5 selected">`)
	})

	r.createInvite("test-invite-2", false)
	user2 := r.createUser("test-user-2@example.com", "test-user-2", "test-password-2", "test-invite-2")

	t.Run("inheriting CanVote=false from invite", func(t *testing.T) {
		assert.Equal(t, false, user2.Permissions.CanVote)
	})

	cookies2 := r.loginUser("test-user-2@example.com", "test-password-2")

	t.Run("voting should not work (authenticated)", func(t *testing.T) {
		response := r.requestJson("POST", fmt.Sprintf("/playlists/1/entries/%v/votes", entryIDs[0]), gin.H{"score": 3}, cookies2)
		assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
	})

	t.Run("voting should not work (unauthenticated)", func(t *testing.T) {
		response := r.requestJson("POST", fmt.Sprintf("/playlists/1/entries/%v/votes", entryIDs[0]), gin.H{"score": 3}, nil)
		assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
	})
}

func Test404Error(t *testing.T) {
	r := NewTestRouterUser(t)

	t.Run("non-existent route should invoke custom error handling", func(t *testing.T) {
		response := r.requestHTML("GET", "/non-exitent-route", nil, nil)
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Contains(t, response.BodyString(), "<h2>Error: Not Found</h2>")
		assert.Contains(t, response.BodyString(), "/non-exitent-route could not be found")
	})
}
