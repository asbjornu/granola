package models

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"math/big"
	"net/url"
	"slices"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/skip2/go-qrcode"
	"gorm.io/gorm"
)

type Invite struct {
	UUIDModel
	Key         string          `gorm:"not null;default:null;uniqueIndex:idx_uq_invites_key"`
	Permissions UserPermissions `gorm:"embedded"`
	UserID      uuid.NullUUID   `gorm:"uniqueIndex:idx_uq_invites_userid"`
	User        User
}

func (invite *Invite) BeforeCreate(tx *gorm.DB) error {
	if invite.ID == uuid.Nil {
		invite.ID = uuid.New()
	}
	return nil
}

func NewInvite(db *gorm.DB, key string, permissions *UserPermissions) (*Invite, error) {
	invite := &Invite{
		Key:         key,
		Permissions: *permissions,
	}
	result := db.Create(invite)
	return invite, result.Error
}

func (invite *Invite) Path() string {
	return fmt.Sprintf("/invites/%v", invite.ID)
}

func (invite *Invite) MarshalJSON() ([]byte, error) {
	h := gin.H{
		"id":      invite.Path(),
		"key":     invite.Key,
		"canVote": invite.Permissions.CanVote,
	}

	if invite.UserID.Valid {
		h["user"] = invite.User.Path()
	}

	return json.Marshal(h)
}

func (invite *Invite) RegisterURL(baseURL string) string {
	if b, err := url.Parse(baseURL); err != nil {
		panic(err)
	} else if u, err := url.Parse("/register?invite=" + url.PathEscape(invite.Key)); err != nil {
		panic(err)
	} else {
		return b.ResolveReference(u).String()
	}
}

func (invite *Invite) QrCode(baseURL string) ([]byte, error) {
	return qrcode.Encode(invite.RegisterURL(baseURL), qrcode.Medium, 256)
}

func GenerateCharset(lowercase bool, uppercase bool, digits bool, unambiguous bool) []rune {
	var charset []rune
	if lowercase {
		for ch := 'a'; ch <= 'z'; ch++ {
			charset = append(charset, ch)
		}
	}
	if uppercase {
		for ch := 'A'; ch <= 'Z'; ch++ {
			charset = append(charset, ch)
		}
	}
	if digits {
		for ch := '0'; ch <= '9'; ch++ {
			charset = append(charset, ch)
		}
	}

	if unambiguous {
		var unambiguousCharset []rune
		ambigiousPairs := [][]rune{
			[]rune{'A', '4'},
			[]rune{'O', '0'},
			[]rune{'S', '5'},
			[]rune{'B', '8'},
			[]rune{'b', '6'},
			[]rune{'G', '6'},
			[]rune{'l', 'I', '1'},
			[]rune{'z', 'Z'},
			[]rune{'Z', '2'},
		}

		for _, ch := range charset {
			kill := false
			for _, ambigiousPair := range ambigiousPairs {
				if !slices.Contains(ambigiousPair, ch) {
					continue
				}

				for _, ch2 := range ambigiousPair {
					if ch != ch2 && slices.Contains(charset, ch2) {
						kill = true
						break
					}
				}
			}

			if !kill {
				unambiguousCharset = append(unambiguousCharset, ch)
			}
		}
		return unambiguousCharset
	}

	return charset
}

func GenerateInvites(db *gorm.DB, amount int, prefix string, charset []rune, digits int, suffix string, canVote bool) ([]Invite, error) {

	randomString := func(digits int, charset []rune) (string, error) {
		var ret []rune
		bigInt := big.NewInt(int64(len(charset) - 1))
		for i := 0; i < digits; i++ {
			idx, err := rand.Int(rand.Reader, bigInt)
			if err != nil {
				return "", err
			}
			ret = append(ret, charset[idx.Int64()])
		}
		return string(ret), nil
	}

	var invites []Invite
	for i := 0; i < amount; i++ {
		key, err := randomString(digits, charset)
		if err != nil {
			return nil, err
		}

		invite, err := NewInvite(db, prefix+key+suffix, &UserPermissions{CanVote: canVote})
		if err != nil {
			return nil, err
		}
		invites = append(invites, *invite)
	}

	return invites, nil
}
