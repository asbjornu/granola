package models

import (
	"encoding/json"

	"github.com/dustin/go-humanize"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gorm.io/gorm"

	"granola/problems"
)

type Vote struct {
	gorm.Model
	EntryID uuid.UUID `gorm:"index:votes_entry_id_user_id;not null;default:null"`
	Entry   Entry
	UserID  uuid.UUID `gorm:"index:votes_entry_id_user_id;not null;default:null"`
	User    User
	Score   uint `gorm:"not null;default:0;check:score <= 5"`
}

func NewVote(db *gorm.DB, entry *Entry, user *User, score uint) (*Vote, error) {
	if !user.Permissions.CanVote {
		return nil, problems.Unauthorized("voting not allowed")
	}

	vote := &Vote{
		EntryID: entry.ID,
		UserID:  user.ID,
		Score:   score,
	}
	if err := db.Create(vote).Error; err != nil {
		return nil, err
	}
	return vote, nil
}

type VotingPlaylist struct {
	Playlist
	Entries []VotingEntry
}

type VotingEntry struct {
	PlaylistEntry
	IsCurrent bool
	Vote      *Vote
}

type VoteTotals struct {
	Votes int
	Score int
}

func (vote *Vote) AsMap() gin.H {
	return gin.H{
		"entry": vote.Entry.Path(),
		"user":  vote.User.Path(),
		"score": vote.Score,
		"created": gin.H{
			"dateTime":  vote.CreatedAt,
			"humanized": humanize.Time(vote.CreatedAt),
		},
	}
}

func (vote *Vote) MarshalJSON() ([]byte, error) {
	return json.Marshal(vote.AsMap())
}

func (votingEntry *VotingEntry) MarshalJSON() ([]byte, error) {
	h := votingEntry.PlaylistEntry.AsMap()

	if votingEntry.Vote != nil {
		h["vote"] = votingEntry.Vote.AsMap()
	}

	return json.Marshal(h)
}

func GetVoteTotals(db *gorm.DB) (*VoteTotals, error) {
	var voteTotals *VoteTotals

	err := db.Raw(`
		select  count(*)   as votes
		,       sum(score) as score
		from    (
			select   score
			from     votes
			group by user_id
			having   created_at = max(created_at)
		)
	`).Scan(&voteTotals).Error

	if voteTotals == nil && err == nil {
		err = gorm.ErrRecordNotFound
	}

	return voteTotals, err
}
