package models

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/yuin/goldmark"
	"gorm.io/gorm"
)

type CompoType string

const (
	CompoTypeUndefined CompoType = ""
	CompoTypeGraphics  CompoType = "graphics"
	CompoTypeMusic     CompoType = "music"
	CompoTypeRealtime  CompoType = "realtime"
)

func (compoType *CompoType) Scan(v any) error {
	value := strings.ToLower(fmt.Sprintf("%s", v))
	for _, ct := range CompoTypes {
		if value == string(ct) {
			*compoType = ct
			return nil
		}
	}

	*compoType = CompoTypeUndefined
	log.Printf("Unknown compo type: %v", v)
	// If we return an error here, it's going to be difficult to load compos
	// with stale compo types from the database. It's better to just convert
	// them to CompoTypeUndefined silently.
	return nil
}

func (compoType *CompoType) IsValid() bool {
	return *compoType != CompoTypeUndefined
}

var CompoTypes = []CompoType{
	CompoTypeUndefined,
	CompoTypeGraphics,
	CompoTypeMusic,
	CompoTypeRealtime,
}

type Compo struct {
	gorm.Model
	Name          string `gorm:"default:null;not null;uniqueIndex:idx_uq_compos_name"`
	MultiPlatform bool
	Locked        bool
	Type          CompoType `gorm:"nullable;default:null"`
	Description   string
	Entries       []Entry
}

func (compo *Compo) Path() string {
	return fmt.Sprintf("/compos/%d", compo.ID)
}

func (compo *Compo) ArchivePath() string {
	return fmt.Sprintf("%v/archive", compo.Path())
}

func (compo *Compo) DescriptionHtml() template.HTML {
	if compo.Description == "" {
		return ""
	}

	md := []byte(compo.Description)
	var b bytes.Buffer
	if err := goldmark.Convert(md, &b); err != nil {
		panic(err)
	}

	return template.HTML(b.Bytes())
}

func NewCompo(db *gorm.DB, name string, multiPlatform bool, locked bool, ct CompoType, description string) (*Compo, error) {
	compo := &Compo{
		Name:          name,
		MultiPlatform: multiPlatform,
		Locked:        locked,
		Type:          ct,
		Description:   description,
	}

	result := db.Create(compo)
	return compo, result.Error
}

func (compo *Compo) MarshalJSON() ([]byte, error) {
	data := gin.H{
		"id":            compo.Path(),
		"name":          compo.Name,
		"multiPlatform": compo.MultiPlatform,
		"locked":        compo.Locked,
	}

	if compo.Type != CompoTypeUndefined {
		data["type"] = compo.Type
	}

	if compo.Description != "" {
		data["description"] = compo.Description
	}

	return json.Marshal(data)
}
