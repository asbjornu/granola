package models

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserPermissions struct {
	CanVote bool
}

type User struct {
	UUIDModel
	Email        string          `gorm:"default:null;not null;uniqueIndex:idx_uq_users_email"`
	Permissions  UserPermissions `gorm:"embedded"`
	Entries      []Entry         `gorm:"foreignKey:UserID"`
	Handle       string          `gorm:"default:null;not null;"`
	PasswordHash string
}

var InvalidInvite = errors.New("Invalid invitation")

func (user *User) BeforeCreate(tx *gorm.DB) error {
	if user.ID == uuid.Nil {
		user.ID = uuid.New()
	}
	return nil
}

func NewUser(db *gorm.DB, email string, handle string, password *string, permissions *UserPermissions) (*User, error) {
	user := &User{Email: email, Handle: handle, Permissions: *permissions}
	if password != nil {
		if err := user.SetPassword(*password); err != nil {
			return nil, err
		}
	}

	if err := db.Create(user).Error; err != nil {
		return nil, err
	}

	return user, nil
}

func NewUserWithInvite(db *gorm.DB, email string, handle string, password string, key string) (*User, error) {
	var user *User
	if err := db.Transaction(func(tx *gorm.DB) error {
		invite := Invite{}
		result := tx.First(&invite, "key = ?", key)
		if result.Error != nil {
			return InvalidInvite
		}

		var err error
		user, err = NewUser(tx, email, handle, &password, &invite.Permissions)
		if err != nil {
			return err
		}

		result = tx.Model(&Invite{}).Where("key = ? AND user_id is null", key).Update("UserID", user.ID)
		if result.RowsAffected == 0 {
			return InvalidInvite
		}

		return result.Error
	}); err != nil {
		return nil, err
	} else {
		return user, nil
	}
}

func (user *User) Path() string {
	return fmt.Sprintf("/users/%v", user.ID)
}

func (user *User) SetPassword(plainTextPassword string) error {
	password := []byte(plainTextPassword)

	hashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	user.PasswordHash = string(hashedPassword)

	return nil
}

func (user *User) CheckPassword(plainTextPassword string) bool {
	if user.PasswordHash == "" {
		return false
	}

	password := []byte(plainTextPassword)
	hashedPassword := []byte(user.PasswordHash)
	err := bcrypt.CompareHashAndPassword(hashedPassword, password)
	return err == nil
}

func (user *User) MarshalJSON() ([]byte, error) {
	h := gin.H{
		"id":      user.Path(),
		"email":   user.Email,
		"handle":  user.Handle,
		"canVote": user.Permissions.CanVote,
	}

	if user.Entries != nil && len(user.Entries) > 0 {
		entries := make([]gin.H, len(user.Entries))
		for i, entry := range user.Entries {
			entries[i] = entry.AsMap()
		}
		h["entries"] = entries
	}

	return json.Marshal(h)
}

func (user *User) Votes(db *gorm.DB, entries []uuid.UUID) ([]*Vote, error) {
	if user == nil {
		return nil, nil
	}

	var votes []*Vote
	err := db.
		Preload("Entry").
		Preload("User").
		Where("user_id = ?", user.ID).
		Where("entry_id in (?)", entries).
		Group("entry_id").
		Having("created_at = max(created_at)").
		Find(&votes).
		Error

	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, err
	}

	return votes, err
}

func (user *User) CurrentVote(db *gorm.DB, entryID uuid.UUID) (*Vote, error) {
	var vote Vote
	result := db.
		Preload("Entry").
		Preload("User").
		Order("created_at desc").
		Where("user_id = ?", user.ID).
		Where("entry_id = ?", entryID).
		First(&vote)
	return &vote, result.Error
}

func (user *User) CurrentVotingPlaylist(db *gorm.DB, reverse bool) (*VotingPlaylist, error) {
	order := "asc"
	if reverse {
		order = "desc"
	}

	var playlist *Playlist
	result := db.
		Preload("Entries", func(tx *gorm.DB) *gorm.DB {
			return tx.Where("shown_at is not null").Order(fmt.Sprintf("`order` %s", order))
		}).
		Preload("Entries.Playlist").
		Preload("Entries.Entry").
		Preload("Entries.Entry.Compo").
		Preload("Entries.Entry.User").
		Order("shown_at desc").
		Where("shown_at is not null").
		First(&playlist)

	entries := playlist.EntryIDs()
	votes, err := user.Votes(db, entries)

	if err != nil {
		return nil, err
	}

	return playlist.VotingPlaylist(votes), result.Error
}
