package models

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gocloud.dev/blob"
	"gorm.io/gorm"
)

type Upload struct {
	UUIDModel
	EntryID     uuid.UUID `gorm:"not null;default:null"`
	Entry       Entry
	Filename    string `gorm:"not null;default:null"`
	ContentSize int64
	ContentType string `gorm:"not null;default:null"`
}

func (upload *Upload) BeforeCreate(tx *gorm.DB) error {
	if upload.ID == uuid.Nil {
		upload.ID = uuid.New()
	}
	return nil
}

func NewUploadFromReader(db *gorm.DB, bucket *blob.Bucket, filename string, contentSize int64, reader io.Reader, entry *Entry) (*Upload, error) {
	// allocate UUID for upload
	id := uuid.New()

	ctx := context.Background()
	w, err := bucket.NewWriter(ctx, fmt.Sprintf("%v", id), nil)
	if err != nil {
		return nil, err
	}

	// Peek at the upload for the content type. This also copies the
	// read bytes to the bucket
	tee := io.TeeReader(reader, w)
	buffer := make([]byte, 512)
	read, err := tee.Read(buffer)
	if err != nil {
		if err != io.EOF {
			return nil, err
		}
	}

	// write the rest of the file to the bucket
	if _, err := io.Copy(w, reader); err != nil {
		return nil, err
	}
	err = w.Close()
	if err != nil {
		return nil, err
	}

	upload := &Upload{
		UUIDModel: UUIDModel{
			ID: id,
		},
		Filename:    filename,
		ContentType: http.DetectContentType(buffer[:read]),
		ContentSize: contentSize,
		Entry:       *entry,
	}

	result := db.Create(&upload)

	return upload, result.Error
}

func NewUploadFromData(db *gorm.DB, bucket *blob.Bucket, filename string, content []byte, entry *Entry) (*Upload, error) {
	return NewUploadFromReader(db, bucket, filename, int64(len(content)), bytes.NewReader(content), entry)
}

func (upload *Upload) Open(bucket *blob.Bucket) (*blob.Reader, error) {
	ctx := context.Background()
	return bucket.NewReader(ctx, fmt.Sprintf("%v", upload.ID), nil)
}

func (upload *Upload) IsImage() bool {
	return strings.HasPrefix(upload.ContentType, "image/")
}

func (upload *Upload) IsAudio() bool {
	return strings.HasPrefix(upload.ContentType, "audio/")
}

func (upload *Upload) IsVideo() bool {
	return strings.HasPrefix(upload.ContentType, "video/")
}

func (upload *Upload) Path() string {
	return fmt.Sprintf("%v/uploads/%v", upload.Entry.Path(), upload.ID)
}

func (upload *Upload) MarshalJSON() ([]byte, error) {
	return json.Marshal(gin.H{
		"id":          upload.Path(),
		"entry":       upload.Entry.Path(),
		"filename":    upload.Filename,
		"contentType": upload.ContentType,
	})
}
