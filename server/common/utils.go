package common

import (
	"fmt"
	"granola/problems"
	"strings"

	"github.com/gin-gonic/gin"
	ut "github.com/go-playground/universal-translator"
	"gorm.io/gorm"
)

var (
	MIMEProblemJSON  = "application/problem+json"
	OfferedMIMETypes = []string{gin.MIMEJSON, MIMEProblemJSON, gin.MIMEHTML}
	quoteEscaper     = strings.NewReplacer("\\", "\\\\", `"`, "\\\"")
)

func EscapeQuotes(s string) string {
	return quoteEscaper.Replace(s)
}

func BaseTransact(db *gorm.DB, t ut.Translator, f func(c *gin.Context, tx *gorm.DB) error) gin.HandlerFunc {
	return func(c *gin.Context) {
		if err := db.Transaction(func(tx *gorm.DB) error {
			return f(c, tx)
		}); err != nil {
			HandleError(c, t, err)
		}
	}
}

func Show404(c *gin.Context) {
	handleProblem(c, problems.NotFound(fmt.Sprintf("We searched everywhere, but %s could not be found.", c.Request.URL.Path)))
}
