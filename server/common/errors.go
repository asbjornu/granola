package common

import (
	"errors"
	"fmt"
	"granola/problems"
	"log"
	"strings"

	"github.com/gin-gonic/gin"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

func HandleError(c *gin.Context, t ut.Translator, a any) {
	problem := translateError(t, a)
	handleProblem(c, problem)
}

func translateError(t ut.Translator, a any) problems.Problem {
	if validationErrors, ok := a.(validator.ValidationErrors); ok {
		text := validationErrors[0].Translate(t)
		return problems.BadRequest(text)
	} else if err, ok := a.(error); ok {
		var problem problems.Problem

		switch {
		case errors.As(err, &problem):
			return problem
		case errors.Is(err, gorm.ErrRecordNotFound):
			return problems.NotFound("")
		default:
			return problems.BadRequest(err.Error())
		}
	}

	return problems.InternalServerError("")
}

func handleProblem(c *gin.Context, problem problems.Problem) {
	accept := c.Request.Header.Get("Accept")

	if strings.Contains(accept, MIMEProblemJSON) {
		// As we can't provide server-side preference for 'application/problem+json',
		// we need to explicitly search for client-side 'Accept' for it and set the
		// content type accordingly.
		c.Header("Content-Type", fmt.Sprintf("%s; charset=utf-8", MIMEProblemJSON))
		c.JSON(problem.StatusCode(), &problem)
	} else if problem.ViewName() != "" {
		c.Negotiate(problem.StatusCode(), gin.Negotiate{
			Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
			HTMLName: problem.ViewName(),
			HTMLData: problem.ViewData(),
			JSONData: &problem,
		})
	} else {
		c.Negotiate(problem.StatusCode(), gin.Negotiate{
			Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
			HTMLName: "error",
			HTMLData: gin.H{
				"Title":   "Error",
				"Problem": problem.AsMap(),
			},
			Data: &problem,
		})
	}

	log.Print(problem.Detail())
	c.Abort()
}
