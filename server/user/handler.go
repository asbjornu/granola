package user

import (
	"io"
	"log"
	"net/http"
	"sync/atomic"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gocloud.dev/blob"
	"gorm.io/gorm"

	"granola/config"
	"granola/models"
	"granola/problems"
	"granola/server/common"

	ut "github.com/go-playground/universal-translator"
)

type Handler struct {
	Translator       ut.Translator
	Config           config.Config
	DB               *gorm.DB
	Bucket           *blob.Bucket
	PlaylistStreamer PlaylistStreamer
}

func (h *Handler) transact(f func(c *gin.Context, tx *gorm.DB) error) gin.HandlerFunc {
	return common.BaseTransact(h.DB, h.Translator, f)
}

func getUser(c *gin.Context) models.User {
	return c.MustGet("user").(models.User)
}

func requireAuth(c *gin.Context) {
	user, _ := c.Get("user")
	if user == nil {
		if c.Request.Method == "GET" {
			c.Redirect(http.StatusTemporaryRedirect, "/login")
			c.Abort()
			return
		}

		panic(problems.Unauthorized("User not logged in"))
		return
	}
	c.Next()
}

func forbidAuth(c *gin.Context) {
	user, _ := c.Get("user")
	if user != nil {
		c.Redirect(http.StatusTemporaryRedirect, "/")
		c.Abort()
		return
	}
	c.Next()
}

func requireCanVote(c *gin.Context) {
	user := getUser(c)
	if user.Permissions.CanVote == false {
		panic(problems.Unauthorized("User not allowed to vote"))
	}
	c.Next()
}

func (h *Handler) CheckAuth(c *gin.Context) {
	session := sessions.Default(c)
	if sessionUser := session.Get("user"); sessionUser != nil {
		user := models.User{}
		result := h.DB.First(&user, sessionUser)
		if result.Error != nil {
			session.Delete("user")
			if err := session.Save(); err != nil {
				panic(err)
			}
		} else {
			c.Set("user", user)
		}
	}
	c.Next()
}

func (h *Handler) showHome(c *gin.Context, tx *gorm.DB) error {
	var user *models.User
	tmp, _ := c.Get("user")
	if tmp2, ok := tmp.(models.User); ok {
		user = &tmp2
		if err := tx.Where("user_id = ?", user.ID).Find(&user.Entries).Error; err != nil {
			return err
		}
	}

	var compos []models.Compo
	if err := tx.Where("locked = false").Find(&compos).Error; err != nil {
		return err
	}

	data := gin.H{
		"Config": &h.Config.Routing,
		"Title":  "Home",
		"User":   user,
		"Compos": compos,
	}
	c.HTML(http.StatusOK, "user/home", data)

	return nil
}

func rateLimit(max uint32) gin.HandlerFunc {
	var count uint32
	return func(c *gin.Context) {
		curr := atomic.AddUint32(&count, 1)
		defer atomic.AddUint32(&count, ^uint32(0))

		if curr <= max {
			c.Next()
		} else {
			c.Header("Retry-After", "1")
			c.Status(http.StatusTooManyRequests)
		}
	}
}

func (h *Handler) getSlideStream(c *gin.Context) {
	c.Writer.Header().Set("Content-Type", "text/event-stream")
	c.Writer.Header().Set("Cache-Control", "no-cache")
	c.Writer.Header().Set("Connection", "keep-alive")
	c.Writer.Header().Set("Transfer-Encoding", "chunked")

	clientChan := make(PlaylistStreamChan)
	h.PlaylistStreamer.NewClient <- clientChan
	defer func() {
		h.PlaylistStreamer.ClosedClient <- clientChan
	}()

	// dummy message to make sure stream is alive
	c.SSEvent("message", "")
	c.Writer.Flush()

	c.Stream(func(w io.Writer) bool {
		if playlistEntry, ok := <-clientChan; ok {
			if bytes, err := playlistEntry.MarshalJSON(); err != nil {
				log.Printf("Error converting Entry '%v' to JSON: %v", playlistEntry.EntryID, err)
			} else {
				message := string(bytes)
				c.SSEvent("message", message)
			}
		}

		return true
	})
}

func (h *Handler) registerPublicRoutes(root *gin.RouterGroup) {
	root.GET("", h.transact(h.showHome))
	root.GET("tribute", func(c *gin.Context) {
		c.Redirect(http.StatusPermanentRedirect, "http://www.slengpung.com/?id=7623")
	})

	// needed for slides
	root.GET("slide-stream", h.getSlideStream)
	root.GET("/playlists/:playlist_id/entries/:entry_id", h.getPlaylistEntryPreview)

	register := root.Group("/register")
	register.Use(forbidAuth)
	register.GET("", h.showRegister)
	register.POST("", h.transact(h.postUser), rateLimit(h.Config.RateLimit.Register))

	login := root.Group("/login")
	login.Use(forbidAuth)
	login.GET("", h.showLogin)
	login.POST("", h.postLogin, rateLimit(h.Config.RateLimit.Login))
}

func (h *Handler) registerPrivateRoutes(private *gin.RouterGroup) {
	private.POST("/logout", h.logoutUser)

	profile := private.Group("/profile")
	profile.GET("", h.getProfile)
	profile.PATCH("", h.transact(h.patchProfile))

	entries := private.Group("/entries")
	entries.GET("", h.transact(h.getEntries))
	entries.POST("", h.transact(h.postEntry))

	entry := entries.Group("/:entry_id")
	entry.GET("", h.getEntry)
	entry.GET("/preview", h.getEntryPreview)
	entry.PUT("", h.transact(h.putEntry))
	entry.DELETE("", h.deleteEntry)

	uploads := entry.Group("/uploads")
	uploads.POST("", h.transact(h.postUpload))

	upload := uploads.Group("/:upload_id")
	upload.GET("", h.getUpload)
	upload.DELETE("", h.transact(h.deleteUpload))
	upload.GET("/data", h.getUploadData)

	voting := private.Group("/voting")
	voting.Use(requireCanVote)
	voting.GET("", h.transact(h.getVoting))
	voting.GET("/live", h.transact(h.getLiveVoting))
	voting.GET("/:playlist_id", h.transact(h.getPlaylistVoting))
	voting.GET("/live/stream", h.getLiveVotingStream)

	private.POST("/playlists/:playlist_id/entries/:entry_id/votes", requireCanVote, h.transact(h.postVote))
}

func (h *Handler) RegisterRoutes(r *gin.Engine) {
	root := r.Group("/")
	h.registerPublicRoutes(root)

	private := root.Group("/")
	private.Use(requireAuth)
	h.registerPrivateRoutes(private)
}
