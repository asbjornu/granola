package user

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"granola/models"
	"granola/problems"
	"granola/server/common"
)

func (h *Handler) getEntries(c *gin.Context, tx *gorm.DB) error {
	var entries []models.Entry
	user := getUser(c)
	if err := tx.
		Preload("Compo").
		Preload("User").
		Preload("Uploads").
		Where("user_id = ?", user.ID).
		Find(&entries).
		Error; err != nil {
		return err
	}

	var compos []models.Compo
	if err := tx.Where("locked = false").Find(&compos).Error; err != nil {
		return err
	}

	var selectedCompo *models.Compo
	tx.Preload("Entries").First(&selectedCompo, c.Query("compo"))

	data := gin.H{
		"Config":        &h.Config.Routing,
		"Title":         "Entries",
		"Entries":       entries,
		"Compos":        compos,
		"SelectedCompo": selectedCompo,
		"User":          &user,
		"HttpContext":   c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "user/entries",
		HTMLData: &data,
		Data:     entries,
	})

	return nil
}

func (h *Handler) postEntry(c *gin.Context, tx *gorm.DB) error {
	args := struct {
		Title    string `json:"title" form:"title" binding:"required"`
		Author   string `json:"author" form:"author" binding:"required"`
		Platform string `json:"platform" form:"platform"`
		Notes    string `json:"notes" form:"notes"`
		CompoID  int    `json:"compo" form:"compo" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	var compo *models.Compo
	if err := tx.Where("locked = false").First(&compo, args.CompoID).Error; err != nil {
		return problems.NotFound(fmt.Sprintf("compo %d not found", args.CompoID))
	}

	user := getUser(c)

	entry, err := models.NewEntry(tx, args.Title, args.Author, args.Platform, args.Notes, compo, &user, false)
	if err != nil {
		return problems.InternalServerError(err.Error())
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, entry.Path())
	} else {
		c.JSON(http.StatusCreated, &entry)
	}

	return nil
}

func (h *Handler) getEntry(c *gin.Context) {
	user := getUser(c)
	entry := models.Entry{}
	result := h.DB.
		Preload("Uploads").
		Preload("Compo").
		Preload("User").
		Preload("Uploads.Entry").
		Where("user_id = ?", user.ID).
		First(&entry, "id = ?", c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Entry",
		"Entry":       &entry,
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "user/entry",
		HTMLData: &data,
		Data:     &entry,
	})
}

func (h *Handler) getEntryPreview(c *gin.Context) {
	user := getUser(c)
	entry := models.Entry{}
	result := h.DB.
		Preload("Compo").
		Preload("User").
		Preload("Uploads.Entry").
		Where("user_id = ?", user.ID).
		First(&entry, "id = ?", c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Entry Preview",
		"Entry":       &entry,
		"EntryNumber": "??",
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "slides/entry", data)
}

func (h *Handler) getPlaylistEntryPreview(c *gin.Context) {
	playlistEntry := models.PlaylistEntry{}
	if err := h.DB.
		Preload("Playlist").
		Preload("Entry.User").
		Preload("Entry.Uploads").
		Preload("Entry.Compo").
		Where("playlist_id = ?", c.Param("playlist_id")).
		Where("entry_id = ?", c.Param("entry_id")).
		Where("shown_at <= ?", time.Now()).
		First(&playlistEntry).
		Error; err != nil {
		panic(err)
	}

	var user *models.User = nil
	tmp, exists := c.Get("user")
	if exists {
		tmpUser := tmp.(models.User)
		user = &tmpUser
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Playlist Preview",
		"Entry":       &playlistEntry.Entry,
		"EntryNumber": playlistEntry.Order,
		"Playlist":    &playlistEntry.Playlist,
		"User":        user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "slides/entry", data)
}

func (h *Handler) putEntry(c *gin.Context, tx *gorm.DB) error {
	var entry *models.Entry
	if err := tx.
		Preload("Uploads").
		Preload("Compo").
		Preload("User").
		Where("user_id = ? AND locked = false", getUser(c).ID).
		First(&entry, "id = ?", c.Param("entry_id")).
		Error; err != nil {
		return err
	}

	args := struct {
		Title    string `json:"title" form:"title" binding:"required"`
		Author   string `json:"author" form:"author" binding:"required"`
		Platform string `json:"platform" form:"platform"`
		Notes    string `json:"notes" form:"notes"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	entry.Title = args.Title
	entry.Author = args.Author
	entry.Platform = args.Platform
	entry.Notes = args.Notes

	if err := tx.Omit("Locked").Save(&entry).Error; err != nil {
		return problems.InternalServerError(err.Error())
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, entry.Path())
	} else {
		c.JSON(http.StatusOK, &entry)
	}

	return nil
}

func (h *Handler) deleteEntry(c *gin.Context) {
	result := h.DB.
		Where("user_id = ? AND locked = false", getUser(c).ID).
		Delete(&models.Entry{}, "id = ?", c.Param("entry_id"))

	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "/entries")
	} else {
		c.Status(http.StatusNoContent)
	}
}
