package user

import (
	"errors"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"granola/models"
	"granola/server/common"
)

func (h *Handler) postVote(c *gin.Context, tx *gorm.DB) error {
	args := struct {
		Score uint `json:"score" form:"score" binding:"required,max=5"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	user := getUser(c)
	playlistEntry := models.PlaylistEntry{}
	if err := tx.
		Preload("Entry").
		Where("playlist_id = ?", c.Param("playlist_id")).
		Where("entry_id = ?", c.Param("entry_id")).
		First(&playlistEntry).
		Error; err != nil {
		return err
	}

	vote, err := models.NewVote(tx, &playlistEntry.Entry, &user, args.Score)
	if err != nil {
		return err
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "/voting")
	} else {
		c.JSON(http.StatusCreated, &vote)
	}

	return nil
}

func (h *Handler) getVoting(c *gin.Context, tx *gorm.DB) error {
	user := getUser(c)

	var playlist *models.Playlist
	result := tx.
		Order("shown_at desc").
		Where("shown_at is not null").
		First(&playlist)

	if result.Error != nil {
		playlist = nil
		if !errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return result.Error
		}
	}

	var playlists []models.Playlist
	if err := tx.
		Where("shown_at is not null").
		Where("live_voting_closed_at is not null").
		Where("voting_closed_at is null").
		Find(&playlists).
		Error; err != nil {
		return err
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Playlist":    playlist,
		"Playlists":   playlists,
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}

	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "user/voting",
		HTMLData: &data,
		Data:     playlist,
	})

	return nil
}

func (h *Handler) getLiveVoting(c *gin.Context, tx *gorm.DB) error {
	user := getUser(c)

	playlist, err := user.CurrentVotingPlaylist(tx, true)
	if err != nil {
		playlist = nil
		if !errors.Is(err, gorm.ErrRecordNotFound) {
			return err
		}
	}

	var playlists []models.Playlist
	if err := tx.
		Where("shown_at is not null").
		Where("live_voting_closed_at is not null").
		Where("voting_closed_at is null").
		Find(&playlists).
		Error; err != nil {
		return err
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Playlist":    playlist,
		"Playlists":   playlists,
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}

	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "user/voting/live",
		HTMLData: &data,
		Data:     playlist,
	})

	return nil
}

func (h *Handler) getPlaylistVoting(c *gin.Context, tx *gorm.DB) error {
	user := getUser(c)

	var playlist *models.Playlist
	if err := tx.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry.Uploads").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id")).
		Error; err != nil {
		return err
	}

	entries := playlist.EntryIDs()
	votes, err := user.Votes(tx, entries)
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	}

	votingPlaylist := playlist.VotingPlaylist(votes)

	var playlists []models.Playlist
	if err := tx.
		Where("shown_at is not null").
		Where("live_voting_closed_at is not null").
		Where("voting_closed_at is null").
		Find(&playlists).
		Error; err != nil {
		return err
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Playlist":    votingPlaylist,
		"Playlists":   playlists,
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}

	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "user/voting/playlist",
		HTMLData: &data,
		Data:     playlist,
	})

	return nil
}

func (h *Handler) currentVotingPlaylistEntry() (*models.PlaylistEntry, error) {
	var user *models.User
	playlist, err := user.CurrentVotingPlaylist(h.DB, false)
	if playlist != nil && len(playlist.Entries) > 0 {
		for _, entry := range playlist.Entries {
			if entry.IsCurrent {
				return &entry.PlaylistEntry, nil
			}
		}
	}

	return nil, err
}

func (h *Handler) StreamEntries() {
	ticker := time.NewTicker(500 * time.Millisecond)
	var currentEntry *models.PlaylistEntry = nil

	for {
		select {
		case client := <-h.PlaylistStreamer.NewClient:
			h.PlaylistStreamer.newClient(client)
			if currentEntry != nil {
				client <- currentEntry
			}

		case client := <-h.PlaylistStreamer.ClosedClient:
			h.PlaylistStreamer.closeClient(client)

		case <-ticker.C:
			if len(h.PlaylistStreamer.ActiveClients) > 0 {
				if entry, err := h.currentVotingPlaylistEntry(); err != nil {
					log.Printf("Error selecting playlist entry for voting: %v", err)
					break
				} else if !currentEntry.Equal(entry) {
					currentEntry = entry
					h.PlaylistStreamer.broadcast(currentEntry)
				}
			}
		}
	}
}

func (h *Handler) getLiveVotingStream(c *gin.Context) {
	c.Writer.Header().Set("Content-Type", "text/event-stream")
	c.Writer.Header().Set("Cache-Control", "no-cache")
	c.Writer.Header().Set("Connection", "keep-alive")
	c.Writer.Header().Set("Transfer-Encoding", "chunked")

	user := getUser(c)
	clientChan := make(PlaylistStreamChan)
	h.PlaylistStreamer.NewClient <- clientChan
	defer func() {
		h.PlaylistStreamer.ClosedClient <- clientChan
	}()

	// dummy message to make sure stream is alive
	c.SSEvent("message", "")
	c.Writer.Flush()

	c.Stream(func(w io.Writer) bool {
		if playlistEntry, ok := <-clientChan; ok {
			vote, _ := user.CurrentVote(h.DB, playlistEntry.EntryID)
			votingEntry := models.VotingEntry{PlaylistEntry: *playlistEntry, Vote: vote}
			if bytes, err := votingEntry.MarshalJSON(); err != nil {
				log.Printf("Error converting Entry '%v' to JSON: %v", playlistEntry.EntryID, err)
			} else {
				message := string(bytes)
				c.SSEvent("message", message)
			}
		}

		return true
	})
}
