package user

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"granola/models"
	"granola/problems"
	"granola/server/common"
)

func (h *Handler) postUpload(c *gin.Context, tx *gorm.DB) error {
	formFile, err := c.FormFile("file")
	if err != nil {
		return problems.BadRequest("file not found in request")
	}

	var entry *models.Entry
	if err := tx.
		Preload("Uploads").
		Where("user_id = ?", getUser(c).ID).
		First(&entry, "id = ?", c.Param("entry_id")).
		Error; err != nil {
		return err
	}

	if entry.Locked {
		return problems.Forbidden("entry has been locked")
	}

	r, err := formFile.Open()
	if err != nil {
		return err
	}

	upload, err := models.NewUploadFromReader(tx, h.Bucket, formFile.Filename, formFile.Size, r, entry)
	if err != nil {
		return err
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, entry.Path())
	} else {
		c.JSON(http.StatusCreated, &upload)
	}

	return nil
}

func (h *Handler) getUpload(c *gin.Context) {
	user := getUser(c)
	upload := models.Upload{}
	result := h.DB.
		Preload("Entry").
		Preload("Entry.Compo").
		Joins("JOIN entries on entries.id = uploads.entry_id").
		Where("entry_id = ?", c.Param("entry_id")).
		Where("entries.user_id = ?", user.ID).
		First(&upload, "uploads.id = ?", c.Param("upload_id"))

	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Upload",
		"Upload":      &upload,
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "user/upload",
		HTMLData: &data,
		Data:     &upload,
	})
}

func (h *Handler) getUploadData(c *gin.Context) {
	upload := models.Upload{}
	result := h.DB.Joins("JOIN entries on entries.id = uploads.entry_id").
		Where("entries.user_id = ?", getUser(c).ID).
		First(&upload, "uploads.id = ?", c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	r, err := upload.Open(h.Bucket)
	if err != nil {
		panic(result.Error)
	}
	defer r.Close()

	c.Header("Content-Disposition", fmt.Sprintf(`attachment; filename="%s"`, common.EscapeQuotes(upload.Filename)))
	c.DataFromReader(http.StatusOK, upload.ContentSize, "application/octet-stream", r, nil)
}

func (h *Handler) deleteUpload(c *gin.Context, tx *gorm.DB) error {
	var upload *models.Upload
	if err := tx.Joins("JOIN entries on entries.id = uploads.entry_id").
		Where("entries.user_id = ?", getUser(c).ID).
		First(&upload, "uploads.id = ?", c.Param("upload_id")).
		Error; err != nil {
		return err
	}

	if err := tx.Delete(&upload).Error; err != nil {
		return err
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.Status(http.StatusNoContent)
	}

	return nil
}
