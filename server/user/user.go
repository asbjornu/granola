package user

import (
	"errors"
	"log"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"granola/models"
	"granola/problems"
	"granola/server/common"
)

func (h *Handler) showRegister(c *gin.Context) {
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Register",
		"Invite":      c.Query("invite"),
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "user/register", data)
}

func (h *Handler) postUser(c *gin.Context, tx *gorm.DB) error {
	args := struct {
		Email    string `json:"email" form:"email" binding:"required,email"`
		Handle   string `json:"handle" form:"handle" binding:"required"`
		Invite   string `json:"invite" form:"invite" binding:"required"`
		Password string `json:"password" form:"password" binding:"required,password"`
	}{}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Register",
		"HttpContext": c.MustGet("HttpContext"),
		"Email":       args.Email,
		"Handle":      args.Handle,
		"Invite":      args.Invite,
	}

	if err := c.ShouldBind(&args); err != nil {
		return problems.UnprocessableContent(err, h.Translator, "user/register", data, nil)
	}

	user, err := models.NewUserWithInvite(tx, args.Email, args.Handle, args.Password, args.Invite)
	if err != nil {
		probs := gin.H{}
		if errors.Is(err, models.InvalidInvite) {
			probs["invite"] = err.Error()
		} else if err.Error() == "UNIQUE constraint failed: users.email" {
			probs["email"] = "Email address already in use"
		} else {
			panic(err)
		}

		data["Problems"] = probs

		return problems.UnprocessableContent(err, h.Translator, "user/register", data, probs)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		session := sessions.Default(c)
		session.Set("user", user.ID)
		if err := session.Save(); err != nil {
			return err
		}
		c.Redirect(http.StatusFound, "/")
	} else {
		c.JSON(http.StatusCreated, &user)
	}

	return nil
}

func (h *Handler) showLogin(c *gin.Context) {
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Login",
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "user/login", data)
}

func (h *Handler) postLogin(c *gin.Context) {
	session := sessions.Default(c)

	args := struct {
		Email    string `json:"email" form:"email" binding:"required"`
		Password string `json:"password" form:"password" binding:"required"`
	}{}

	if err := c.ShouldBind(&args); err != nil {
		panic(problems.Unauthorized(err.Error()))
	}

	user := models.User{}
	if err := h.DB.First(&user, "email = ?", args.Email).Error; err != nil {
		log.Printf("No user found for %s. %v", args.Email, err)
	}
	if !user.CheckPassword(args.Password) {
		message := "Invalid credentials"
		htmlData := gin.H{
			"Config":      &h.Config.Routing,
			"Title":       "Login",
			"Email":       args.Email,
			"Error":       message,
			"HttpContext": c.MustGet("HttpContext"),
		}
		jsonData := gin.H{"detail": message}
		c.Negotiate(http.StatusUnauthorized, gin.Negotiate{
			Offered:  common.OfferedMIMETypes,
			HTMLName: "user/login",
			HTMLData: &htmlData,
			Data:     &jsonData,
		})
		return
	}

	session.Set("user", user.ID)

	if err := session.Save(); err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/")
	} else {
		// It is stupid to use RFC 7807 as a format for a successful
		// response, but I have no good ideas for a better format.
		c.JSON(http.StatusOK, gin.H{"detail": "Successfully authenticated user"})
	}
}

func (h *Handler) logoutUser(c *gin.Context) {
	session := sessions.Default(c)
	session.Delete("user")
	if err := session.Save(); err != nil {
		panic(err)
	}
	c.Redirect(http.StatusFound, "/")
}

func (h *Handler) getProfile(c *gin.Context) {
	user := getUser(c)
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Profile",
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "user/profile",
		HTMLData: &data,
		Data:     &user,
	})
}

func (h *Handler) patchProfile(c *gin.Context, tx *gorm.DB) error {
	args := struct {
		Email       string `json:"email" form:"email" binding:"omitempty,email"`
		Handle      string `json:"handle" form:"handle"`
		OldPassword string `json:"old-password" form:"old-password"`
		Password    string `json:"password" form:"password"`
	}{}
	user := getUser(c)
	if err := c.ShouldBind(&args); err != nil {
		return err
	}
	if args.Email != "" {
		user.Email = args.Email
	}
	if args.Handle != "" {
		user.Handle = args.Handle
	}
	if args.Password != "" {
		if !user.CheckPassword(args.OldPassword) {
			return problems.Unauthorized("invalid credentials")
		}
		if err := user.SetPassword(args.Password); err != nil {
			return err
		}
	}
	if err := tx.Save(&user).Error; err != nil {
		return problems.InternalServerError(err.Error())
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/profile")
	} else {
		c.JSON(http.StatusOK, &user)
	}

	return nil
}
