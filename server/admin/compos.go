package admin

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gosimple/slug"
	"gorm.io/gorm"

	"granola/models"
	"granola/problems"
	"granola/server/common"
)

func (h *Handler) getCompos(c *gin.Context) {
	var compos []models.Compo
	result := h.DB.Preload("Entries").Find(&compos)

	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Compos",
		"Compos":      compos,
		"CompoTypes":  models.CompoTypes,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/compos",
		HTMLData: &data,
		Data:     compos,
	})
}

func (h *Handler) postCompo(c *gin.Context) {
	args := struct {
		Name          string           `json:"name" form:"name" binding:"required"`
		MultiPlatform *bool            `json:"multiPlatform" form:"multiplatform" binding:"required"`
		Locked        *bool            `json:"locked" form:"locked"`
		Type          models.CompoType `json:"type" form:"type"`
		Description   string           `json:"description" form:"description"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	locked := false
	if args.Locked != nil {
		locked = *args.Locked
	}

	compo, err := models.NewCompo(
		h.DB,
		args.Name,
		*args.MultiPlatform,
		locked,
		args.Type,
		args.Description)
	if err != nil {
		// TODO: When this fails with `UNIQUE constraint failed: compos.name`
		//       (sqlite3.Error, Code 19, ExtendedCode 2067) we should preferably
		//       fail with a 409 Conflict response explaining that the compo name
		//       is already taken.
		panic(problems.InternalServerError(err.Error()))
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/compos")
	} else {
		c.JSON(http.StatusCreated, compo)
	}
}

func (h *Handler) getCompo(c *gin.Context) {
	compo := models.Compo{}
	result := h.DB.Preload("Entries.Uploads").First(&compo, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Compo",
		"Compo":       &compo,
		"CompoTypes":  models.CompoTypes,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/compo",
		HTMLData: &data,
		Data:     &compo,
	})
}

func (h *Handler) putCompo(c *gin.Context, tx *gorm.DB) error {
	args := struct {
		Name          string           `json:"name" form:"name" binding:"required"`
		MultiPlatform *bool            `json:"multiPlatform" form:"multiplatform" binding:"required"`
		Locked        *bool            `json:"locked" form:"locked" binding:"required"`
		Type          models.CompoType `json:"type" form:"type"`
		Description   string           `json:"description" form:"description"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	var compo *models.Compo
	if err := tx.First(&compo, c.Param("compo_id")).Error; err != nil {
		return err
	}

	compo.Name = args.Name
	compo.MultiPlatform = *args.MultiPlatform
	compo.Locked = *args.Locked
	compo.Type = args.Type
	compo.Description = args.Description

	if err := tx.Save(&compo).Error; err != nil {
		return problems.InternalServerError(err.Error())
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, compo.Path())
	} else {
		c.JSON(http.StatusOK, &compo)
	}

	return nil
}

func (h *Handler) patchCompo(c *gin.Context) {
	args := struct {
		Locked *bool `json:"locked" form:"locked" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	var compo *models.Compo
	if err := h.DB.Transaction(func(tx *gorm.DB) error {
		if err := tx.First(&compo, c.Param("compo_id")).Error; err != nil {
			return err
		}

		tx.Model(&compo).Update("Locked", args.Locked)

		return nil
	}); err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/compos")
	} else {
		c.JSON(http.StatusOK, compo)
	}
}

func (h *Handler) deleteCompo(c *gin.Context) {
	result := h.DB.Delete(&models.Compo{}, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "/compos")
	} else {
		c.Status(http.StatusNoContent)
	}
}

func (h *Handler) postCompoEntry(c *gin.Context, tx *gorm.DB) error {
	args := struct {
		Title    string `json:"title" form:"title" binding:"required"`
		Author   string `json:"author" form:"author" binding:"required"`
		Platform string `json:"platform" form:"platform"`
		Notes    string `json:"notes" form:"notes"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	var compo *models.Compo
	if err := tx.First(&compo, c.Param("compo_id")).Error; err != nil {
		return err
	}

	entry, err := models.NewEntry(tx, args.Title, args.Author, args.Platform, args.Notes, compo, &models.User{}, false)
	if err != nil {
		return problems.InternalServerError(err.Error())
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, compo.Path())
	} else {
		c.JSON(http.StatusCreated, &entry)
	}

	return nil
}

func (h *Handler) getCompoArchive(c *gin.Context) {
	// TODO: Combine this with getPlaylistArchive() somehow to avoid (this much)
	//       code duplication.
	compo := models.Compo{}
	if err := h.DB.
		Preload("Entries").
		Preload("Entries.Uploads").
		First(&compo, c.Param("compo_id")).
		Error; err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/zip")
	c.Writer.Header().Set("Content-Disposition", "attachment; filename="+slug.Make(compo.Name)+".zip")
	// TODO: Test this. For some reason, invoking c.Stream() causes the following
	//       error in the test suite (but not in the browser):
	//       interface conversion: *httptest.ResponseRecorder is not http.CloseNotifier:
	//       missing method CloseNotify
	c.Stream(func(w io.Writer) bool {
		compoArchive := zip.NewWriter(w)
		defer func(compoArchive *zip.Writer) {
			if err := compoArchive.Close(); err != nil {
				log.Print(err)
			}
		}(compoArchive)

		for _, entry := range compo.Entries {
			if len(entry.Uploads) == 0 {
				// If there are no uploads, skip this entry.
				continue
			}

			if len(entry.Uploads) == 1 {
				// If there's only one upload, don't create a zip file for it.
				upload := entry.Uploads[0]
				ur, err := upload.Open(h.Bucket)
				if err != nil {
					log.Print(err)
					return false
				}
				defer ur.Close()

				if f, err := compoArchive.Create(upload.Filename); err != nil {
					fmt.Print(err)
					return false
				} else if _, err := io.Copy(f, ur); err != nil {
					fmt.Print(err)
					return false
				}
				continue
			}

			func() {
				// Otherwise, create a zip file that contains all uploads for the entry.
				entryBuffer := &bytes.Buffer{}
				entryArchive := zip.NewWriter(entryBuffer)

				entryFilename := fmt.Sprintf("%s-%s", slug.Make(entry.Author), slug.Make(entry.Title))
				for _, upload := range entry.Uploads {
					ur, err := upload.Open(h.Bucket)
					if err != nil {
						log.Print(err)
						return
					}
					defer ur.Close()

					filename := fmt.Sprintf("%s/%s", entryFilename, upload.Filename)
					if f, err := entryArchive.Create(filename); err != nil {
						fmt.Print(err)
						return
					} else if _, err := io.Copy(f, ur); err != nil {
						fmt.Print(err)
						return
					}
				}

				if err := entryArchive.Close(); err != nil {
					fmt.Print(err)
				}

				if f, err := compoArchive.Create(entryFilename + ".zip"); err != nil {
					fmt.Print(err)
				} else if _, err := f.Write(entryBuffer.Bytes()); err != nil {
					fmt.Print(err)
				}
			}()
		}

		return false
	})
}
