package admin

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gocloud.dev/blob"
	"gorm.io/gorm"

	"granola/config"
	"granola/models"
	"granola/server/common"

	ut "github.com/go-playground/universal-translator"
)

type Handler struct {
	Translator ut.Translator
	Config     config.Config
	DB         *gorm.DB
	Bucket     *blob.Bucket
}

func (h *Handler) transact(f func(c *gin.Context, tx *gorm.DB) error) gin.HandlerFunc {
	return common.BaseTransact(h.DB, h.Translator, f)
}

func (h *Handler) showAdmin(c *gin.Context, tx *gorm.DB) error {
	var userCount int64
	var entryCount int64
	if err := tx.Model(&models.User{}).Count(&userCount).Error; err != nil {
		return err
	}
	if err := tx.Model(&models.Entry{}).Count(&entryCount).Error; err != nil {
		return err
	}
	voteTotals, err := models.GetVoteTotals(tx)
	if err != nil {
		return err
	}

	stats := gin.H{
		"users":   userCount,
		"entries": entryCount,
		"votes":   voteTotals.Votes,
		"score":   voteTotals.Score,
	}
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Admin",
		"Stats":       &stats,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/admin",
		HTMLData: &data,
		Data:     &stats,
	})

	return nil
}

func (h *Handler) getResults(c *gin.Context, tx *gorm.DB) error {
	var playlists []models.Playlist
	if err := tx.Preload("Entries").
		Preload("Entries.Entry").
		Find(&playlists).
		Error; err != nil {
		return err
	}

	var results []models.PlaylistResult
	for _, playlist := range playlists {
		if result, err := playlist.GetPlaylistResult(tx); err != nil {
			return err
		} else {
			results = append(results, *result)
		}
	}

	c.Writer.Header().Set("Content-Type", "text/plain; charset=utf-8")
	if _, ok := c.GetQuery("download"); ok {
		c.Header("Content-Disposition", "attachment; filename=results.txt")
	}
	c.HTML(http.StatusOK, "admin/_results", results)

	return nil
}

func (h *Handler) RegisterRoutes(r *gin.Engine, admins map[string]string) {
	admin := r.Group("")
	if len(admins) > 0 {
		admin.Use(gin.BasicAuth(admins))
	}

	admin.GET("", h.transact(h.showAdmin))

	invites := admin.Group("/invites")
	invites.GET("", h.getInvites)
	invites.POST("", h.postInvite)
	invites.POST("/generate", h.generateInvites)

	invite := invites.Group("/:invite_id")
	invite.PATCH("", h.transact(h.patchInvite))
	invite.DELETE("", h.deleteInvite)

	users := admin.Group("/users")
	users.GET("", h.getUsers)
	users.POST("", h.postUser)

	user := users.Group("/:user_id")
	user.GET("", h.getUser)
	user.PATCH("", h.transact(h.patchUser))
	user.DELETE("", h.deleteUser)

	compos := admin.Group("/compos")
	compos.GET("", h.getCompos)
	compos.POST("", h.postCompo)

	compo := compos.Group("/:compo_id")
	compo.GET("", h.getCompo)
	compo.GET("/archive", h.getCompoArchive)
	compo.PUT("", h.transact(h.putCompo))
	compo.PATCH("", h.patchCompo)
	compo.DELETE("", h.deleteCompo)
	compo.POST("/entries", h.transact(h.postCompoEntry))

	entries := admin.Group("/entries")
	entries.GET("", h.getEntries)

	entry := entries.Group("/:entry_id")
	entry.GET("", h.transact(h.getEntry))
	entry.GET("/preview", h.getEntryPreview)
	entry.PUT("", h.transact(h.putEntry))
	entry.DELETE("", h.deleteEntry)

	uploads := entry.Group("/uploads")
	uploads.GET("", h.getUploads)
	uploads.POST("", h.transact(h.postUpload))

	upload := uploads.Group("/:upload_id")
	upload.GET("", h.getUpload)
	upload.DELETE("", h.deleteUpload)
	upload.GET("/data", h.getUploadData)

	playlists := admin.Group("/playlists")
	playlists.GET("", h.getPlaylists)
	playlists.PATCH("", h.transact(h.patchPlaylists))
	playlists.POST("", h.postPlaylist)

	playlist := playlists.Group("/:playlist_id")
	playlist.GET("", h.transact(h.getPlaylist))
	playlist.GET("/archive", h.getPlaylistArchive)
	playlist.GET("/results", h.transact(h.getPlaylistResults))
	playlist.PUT("", h.transact(h.putPlaylist))
	playlist.PATCH("", h.transact(h.patchPlaylist))
	playlist.DELETE("", h.deletePlaylist)

	playlistEntries := playlist.Group("/entries")
	playlistEntries.GET("", h.getPlaylistEntries)
	playlistEntries.POST("", h.transact(h.postPlaylistEntry))

	playlistEntry := playlistEntries.Group("/:entry_id")
	playlistEntry.GET("", h.getPlaylistEntry)
	playlistEntry.PATCH("", h.transact(h.patchPlaylistEntry))
	playlistEntry.DELETE("", h.deletePlaylistEntry)

	playlistPreview := playlist.Group("/preview")
	playlistPreview.GET("", h.getPlaylistPreview)
	playlistPreview.GET("/:slide_number", h.getPlaylistPreview)

	playlistShow := playlist.Group("/show")
	playlistShow.GET("", h.getPlaylistShow)

	playlistShowSlide := playlistShow.Group("/:slide_number")
	playlistShowSlide.GET("", h.getPlaylistShow)
	playlistShowSlide.POST("", h.postPlaylistShowSlide)

	admin.GET("/results", h.transact(h.getResults))
}
