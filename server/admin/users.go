package admin

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"granola/models"
	"granola/problems"
	"granola/server/common"
)

func (h *Handler) getUsers(c *gin.Context) {
	var users []models.User
	result := h.DB.Preload("Entries").Find(&users)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Users",
		"Users":       users,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/users",
		HTMLData: &data,
		Data:     users,
	})
}

func (h *Handler) postUser(c *gin.Context) {
	args := struct {
		Email   string `json:"email" form:"email" binding:"required"`
		Handle  string `json:"handle" form:"handle" binding:"required"`
		CanVote *bool  `json:"canVote" form:"can-vote" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	user, err := models.NewUser(h.DB, args.Email, args.Handle, nil, &models.UserPermissions{CanVote: *args.CanVote})
	if err != nil {
		panic(problems.InternalServerError(err.Error()))
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/users")
	} else {
		c.JSON(http.StatusCreated, &user)
	}
}

func (h *Handler) getUser(c *gin.Context) {
	user := models.User{}
	if err := h.DB.
		Preload("Entries.Uploads").
		First(&user, "id = ?", c.Param("user_id")).
		Error; err != nil {
		panic(err)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "User",
		"User":        &user,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/user",
		HTMLData: &data,
		Data:     &user,
	})
}

func (h *Handler) patchUser(c *gin.Context, tx *gorm.DB) error {
	args := struct {
		Email    string `json:"email" form:"email" binding:"omitempty,email"`
		Handle   string `json:"handle" form:"handle"`
		Password string `json:"password" form:"password"`
		CanVote  *bool  `json:"canVote" form:"can-vote"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	user := models.User{}
	if err := tx.First(&user, "id = ?", c.Param("user_id")).Error; err != nil {
		return err
	}

	if args.Email != "" {
		user.Email = args.Email
	}

	if args.Handle != "" {
		user.Handle = args.Handle
	}

	if args.Password != "" {
		if err := user.SetPassword(args.Password); err != nil {
			return err
		}
	}

	if args.CanVote != nil {
		user.Permissions.CanVote = *args.CanVote
	}

	if err := tx.Save(&user).Error; err != nil {
		return problems.InternalServerError(err.Error())
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, user.Path())
	} else {
		c.JSON(http.StatusOK, &user)
	}

	return nil
}

func (h *Handler) deleteUser(c *gin.Context) {
	if err := h.DB.Delete(&models.User{}, "id = ?", c.Param("user_id")).Error; err != nil {
		panic(problems.InternalServerError(err.Error()))
	}

	c.Status(http.StatusNoContent)
}
