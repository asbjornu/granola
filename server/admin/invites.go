package admin

import (
	"net/http"
	"slices"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"granola/models"
	"granola/problems"
	"granola/server/common"
)

func (h *Handler) getInvites(c *gin.Context) {
	var invites []models.Invite
	result := h.DB.Preload("User").Find(&invites)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Invites",
		"Invites":     invites,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/invites",
		HTMLData: &data,
		Data:     &invites,
	})
}

func (h *Handler) postInvite(c *gin.Context) {
	args := struct {
		Key     string `json:"key" form:"key" binding:"required"`
		CanVote *bool  `json:"canVote" form:"can-vote" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	invite, err := models.NewInvite(h.DB, args.Key, &models.UserPermissions{CanVote: *args.CanVote})
	if err != nil {
		panic(problems.InternalServerError(err.Error()))
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/invites")
	} else {
		c.JSON(http.StatusCreated, invite)
	}
}

func (h *Handler) generateInvites(c *gin.Context) {
	args := struct {
		Amount      int      `json:"amount" form:"amount" binding:"required"`
		Prefix      string   `json:"prefix" form:"prefix"`
		Digits      int      `json:"digits" form:"digits" binding:"required"`
		Classes     []string `json:"classes" form:"classes[]" binding:"required"`
		Suffix      string   `json:"suffix" form:"suffix"`
		CanVote     *bool    `json:"canVote" form:"can-vote" binding:"required"`
		Unambiguous *bool    `json:"unambiguous" form:"unambiguous" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	lowercase := slices.Contains(args.Classes, "lowercase")
	uppercase := slices.Contains(args.Classes, "uppercase")
	digits := slices.Contains(args.Classes, "digits")
	charset := models.GenerateCharset(lowercase, uppercase, digits, *args.Unambiguous)

	invites, err := models.GenerateInvites(h.DB, args.Amount, args.Prefix, charset, args.Digits, args.Suffix, *args.CanVote)
	if err != nil {
		panic(problems.InternalServerError(err.Error()))
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/invites")
	} else {
		c.JSON(http.StatusCreated, invites)
	}
}

func (h *Handler) patchInvite(c *gin.Context, tx *gorm.DB) error {
	args := struct {
		Key     string `json:"key" form:"key"`
		CanVote *bool  `json:"canVote" form:"can-vote"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	invite := models.Invite{}
	if err := tx.First(&invite, "id = ?", c.Param("invite_id")).Error; err != nil {
		return err
	}

	if args.Key != "" {
		invite.Key = args.Key
	}

	if args.CanVote != nil {
		invite.Permissions.CanVote = *args.CanVote
	}

	if err := tx.Save(&invite).Error; err != nil {
		return problems.InternalServerError(err.Error())
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/invites")
	} else {
		c.JSON(http.StatusOK, &invite)
	}

	return nil
}

func (h *Handler) deleteInvite(c *gin.Context) {
	result := h.DB.Delete(&models.Invite{}, "id = ?", c.Param("invite_id"))

	if result.Error != nil {
		panic(problems.InternalServerError(result.Error.Error()))
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "/invites")
	} else {
		c.Status(http.StatusNoContent)
	}
}
