package admin

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"granola/models"
	"granola/problems"
	"granola/server/common"
)

func (h *Handler) getUploads(c *gin.Context) {
	var uploads []models.Upload
	if err := h.DB.
		Preload("Entry").
		Where("entry_id = ?", c.Param("entry_id")).
		Find(&uploads).
		Error; err != nil {
		panic(problems.InternalServerError(err.Error()))
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Uploads",
		"Uploads":     uploads,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/uploads",
		HTMLData: &data,
		Data:     uploads,
	})
}

func (h *Handler) postUpload(c *gin.Context, tx *gorm.DB) error {
	formFile, err := c.FormFile("file")
	if err != nil {
		return err
	}

	var entry *models.Entry
	if err := tx.
		Preload("Uploads").
		First(&entry, "id = ?", c.Param("entry_id")).
		Error; err != nil {
		return err
	}

	r, err := formFile.Open()
	if err != nil {
		return err
	}

	upload, err := models.NewUploadFromReader(tx, h.Bucket, formFile.Filename, formFile.Size, r, entry)
	if err != nil {
		return err
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, upload.Path())
	} else {
		c.JSON(http.StatusCreated, &upload)
	}

	return nil
}

func (h *Handler) getUpload(c *gin.Context) {
	upload := models.Upload{}
	result := h.DB.
		Preload("Entry").
		Preload("Entry.Compo").
		Where("entry_id = ?", c.Param("entry_id")).
		First(&upload, "id = ?", c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Upload",
		"Upload":      &upload,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/upload",
		HTMLData: &data,
		Data:     &upload,
	})
}

func (h *Handler) getUploadData(c *gin.Context) {
	upload := models.Upload{}
	result := h.DB.Where("entry_id = ?", c.Param("entry_id")).
		First(&upload, "id = ?", c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	r, err := upload.Open(h.Bucket)
	if err != nil {
		panic(result.Error)
	}
	defer r.Close()

	c.Header("Content-Disposition", fmt.Sprintf(`attachment; filename="%s"`, common.EscapeQuotes(upload.Filename)))
	c.DataFromReader(http.StatusOK, upload.ContentSize, "application/octet-stream", r, nil)
}

func (h *Handler) deleteUpload(c *gin.Context) {
	result := h.DB.
		Where("entry_id = ?", c.Param("entry_id")).
		Delete(&models.Upload{}, "id = ?", c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.Status(http.StatusNoContent)
	}
}
