package admin

import (
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/gosimple/slug"
	"gorm.io/gorm"

	"granola/models"
	"granola/server/common"
)

func (h *Handler) getPlaylists(c *gin.Context) {
	var playlists []models.Playlist
	result := h.DB.Preload("Entries").Find(&playlists)

	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Playlists",
		"Playlists":   playlists,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/playlists",
		HTMLData: &data,
		Data:     playlists,
	})
}

func (h *Handler) putPlaylist(c *gin.Context, tx *gorm.DB) error {
	args := struct {
		Name string `json:"name" form:"name" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	var playlist *models.Playlist
	if err := tx.First(&playlist, c.Param("playlist_id")).Error; err != nil {
		return err
	}

	playlist.Name = args.Name
	if err := tx.Save(&playlist).Error; err != nil {
		return err
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, playlist.Path()+"?"+c.Request.URL.RawQuery)
	} else {
		c.JSON(http.StatusOK, &playlist)
	}

	return nil
}

func (h *Handler) postPlaylist(c *gin.Context) {
	args := struct {
		Name    string `json:"name" form:"name" binding:"required"`
		Entries []struct {
			EntryID uuid.UUID `json:"entry" binding:"required"`
		} `json:"entries"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	var entries []uuid.UUID
	for _, e := range args.Entries {
		entries = append(entries, e.EntryID)
	}

	playlist, err := models.NewPlaylist(h.DB, args.Name, entries)
	if err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, playlist.Path()+"?"+c.Request.URL.RawQuery)
	} else {
		c.JSON(http.StatusCreated, &playlist)
	}
}

func (h *Handler) patchPlaylists(c *gin.Context, tx *gorm.DB) error {
	type PlaylistAction string
	const (
		Close PlaylistAction = "close"
	)
	args := struct {
		Action PlaylistAction `form:"action" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	var playlists []models.Playlist
	now := time.Now()
	if err := tx.
		Model(&playlists).
		Where("voting_closed_at is null").
		UpdateColumn("voting_closed_at", now).
		Error; err != nil {
		return err
	}

	if err := tx.
		Model(&playlists).
		Where("live_voting_closed_at is null").
		UpdateColumn("live_voting_closed_at", now).
		Error; err != nil {
		return err
	}

	if err := tx.Find(&playlists).Error; err != nil {
		return err
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/playlists")
	} else {
		c.JSON(http.StatusOK, &playlists)
	}

	return nil
}

func (h *Handler) postPlaylistEntry(c *gin.Context, tx *gorm.DB) error {
	args := struct {
		// Can't bind to uuid.UUID because of this:
		// https://github.com/gin-gonic/gin/issues/2673
		EntryID string `json:"entry" form:"entry" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	entryUUID, err := uuid.Parse(args.EntryID)
	if err != nil {
		return err
	}

	var playlist *models.Playlist
	playlistId := c.Param("playlist_id")
	if err := tx.First(&playlist, playlistId).Error; err != nil {
		return err
	}

	var entry *models.Entry
	// A foreign constraint should check this, but I can't seem to get that to work with SQLite.
	if err := tx.First(&entry, "id = ?", entryUUID).Error; err != nil {
		return err
	}

	playlistEntryCount := uint(0)
	if err := tx.
		Table("playlist_entries").
		Where("playlist_id = ?", playlistId).
		Select("count(entry_id)").
		Row().
		Scan(&playlistEntryCount); err != nil {
		playlistEntryCount = 0
	}

	playlistEntry, err := models.NewPlaylistEntry(tx, playlist, entry, playlistEntryCount+1)
	if err != nil {
		return err
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, playlist.Path()+"?"+c.Request.URL.RawQuery+"#add-entry")
	} else {
		c.JSON(http.StatusCreated, &playlistEntry)
	}

	return nil
}

func (h *Handler) patchPlaylistEntry(c *gin.Context, tx *gorm.DB) error {
	type Direction string
	const (
		Up   Direction = "up"
		Down Direction = "down"
	)
	args := struct {
		Direction Direction `json:"direction" form:"direction" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	uriArgs := struct {
		// Can't bind to uuid.UUID because of this:
		// https://github.com/gin-gonic/gin/issues/2673
		EntryID string `uri:"entry_id" binding:"required,uuid"`
	}{}
	if err := c.ShouldBindUri(&uriArgs); err != nil {
		return err
	}

	var playlistEntries []models.PlaylistEntry
	if err := tx.
		Preload("Playlist").
		Preload("Entry").
		Order("`order` asc").
		Find(&playlistEntries, "playlist_id = ?", c.Param("playlist_id")).
		Error; err != nil {
		return err
	}

	index, playlistEntry, err := func() (int, *models.PlaylistEntry, error) {
		u := uuid.MustParse(uriArgs.EntryID)
		for i, entry := range playlistEntries {
			if entry.EntryID == u {
				return i, &entry, nil
			}
		}

		return 0, nil, fmt.Errorf("entry not found")
	}()
	if err != nil {
		return nil
	}

	if reorderedPlaylistEntry := func() *models.PlaylistEntry {
		switch args.Direction {
		case Up:
			if index > 0 {
				return &playlistEntries[index-1]
			}
		case Down:
			if index < len(playlistEntries)-1 {
				return &playlistEntries[index+1]
			}
		}

		return nil
	}(); reorderedPlaylistEntry != nil {
		reorderedPlaylistEntry.Order, playlistEntry.Order = playlistEntry.Order, reorderedPlaylistEntry.Order
		if err := tx.Save(&playlistEntry).Error; err != nil {
			return err
		}
		if err := tx.Save(&reorderedPlaylistEntry).Error; err != nil {
			return err
		}
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, playlistEntry.Playlist.Path()+"?"+c.Request.URL.RawQuery)
	} else {
		c.JSON(http.StatusOK, &playlistEntry)
	}

	return nil
}

func (h *Handler) getPlaylistEntries(c *gin.Context) {
	var playlistEntries []models.PlaylistEntry
	if err := h.DB.
		Preload("Playlist").
		Preload("Entry").
		Order("`order` asc").
		Find(&playlistEntries, "playlist_id = ?", c.Param("playlist_id")).
		Error; err != nil {
		panic(err)
	}
	c.JSON(http.StatusOK, &playlistEntries)
}

func (h *Handler) deletePlaylistEntry(c *gin.Context) {
	result := h.DB.
		Where("playlist_id = ? and entry_id = ?",
			c.Param("playlist_id"),
			c.Param("entry_id")).
		Delete(&models.PlaylistEntry{})
	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.Status(http.StatusNoContent)
	}
}

func (h *Handler) getPlaylist(c *gin.Context, tx *gorm.DB) error {
	var playlist *models.Playlist
	if err := tx.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id")).
		Error; err != nil {
		return err
	}

	var compos []models.Compo
	if err := tx.Find(&compos).Error; err != nil {
		return err
	}

	var selectedCompo *models.Compo
	if err := tx.
		Preload("Entries").
		First(&selectedCompo, c.Query("compo")).
		Error; err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	}

	results, err := playlist.GetPlaylistResult(tx)
	if err != nil {
		return err
	}

	data := gin.H{
		"Config":        &h.Config.Routing,
		"Title":         "Playlist",
		"Playlist":      playlist,
		"Results":       results,
		"Compos":        compos,
		"SelectedCompo": selectedCompo,
		"HttpContext":   c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/playlist",
		HTMLData: &data,
		Data:     &playlist,
	})

	return err
}

func (h *Handler) getPlaylistResults(c *gin.Context, tx *gorm.DB) error {
	playlist := models.Playlist{}
	if err := tx.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id")).
		Error; err != nil {
		return err
	}

	results, err := playlist.GetPlaylistResult(tx)
	if err != nil {
		return err
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Results":     results,
		"HttpContext": c.MustGet("HttpContext"),
	}

	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "slides/playlist-results",
		HTMLData: &data,
		Data:     &results,
	})

	return nil
}

func (h *Handler) getPlaylistArchive(c *gin.Context) {
	// TODO: Combine this with getCompoArchive() somehow to avoid (this much)
	//       code duplication.
	query := struct {
		Ordered bool `form:"ordered"`
	}{}
	if err := c.ShouldBindQuery(&query); err != nil {
		panic(err)
	}

	playlist := models.Playlist{}
	if err := h.DB.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry.Uploads").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id")).
		Error; err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/zip")
	c.Writer.Header().Set("Content-Disposition", "attachment; filename="+slug.Make(playlist.Name)+".zip")
	// TODO: Test this. For some reason, invoking c.Stream() causes the following
	//       error in the test suite (but not in the browser):
	//       interface conversion: *httptest.ResponseRecorder is not http.CloseNotifier:
	//       missing method CloseNotify
	c.Stream(func(w io.Writer) bool {
		playlistArchive := zip.NewWriter(w)
		defer func(playlistArchive *zip.Writer) {
			if err := playlistArchive.Close(); err != nil {
				log.Print(err)
			}
		}(playlistArchive)

		for _, entry := range playlist.Entries {
			if len(entry.Entry.Uploads) == 0 {
				// If there are no uploads, skip this entry.
				continue
			}

			if !query.Ordered && len(entry.Entry.Uploads) == 1 {
				// If we don't want an ordered archive, and if there's only one
				// upload, don't create a zip file for it.
				upload := entry.Entry.Uploads[0]
				ur, err := upload.Open(h.Bucket)
				if err != nil {
					log.Print(err)
					return false
				}
				defer ur.Close()

				if f, err := playlistArchive.Create(upload.Filename); err != nil {
					log.Print(err)
					return false
				} else if _, err := io.Copy(f, ur); err != nil {
					log.Print(err)
					return false
				}
				continue
			}

			func() {
				// Otherwise, create a zip file that contains all uploads for the entry.
				entryBuffer := &bytes.Buffer{}
				entryArchive := zip.NewWriter(entryBuffer)

				entryFilename := fmt.Sprintf("%s-%s", slug.Make(entry.Entry.Author), slug.Make(entry.Entry.Title))

				if query.Ordered {
					entryFilename = fmt.Sprintf("%02d_%s", entry.Order, entryFilename)
				}

				for _, upload := range entry.Entry.Uploads {
					filename := fmt.Sprintf("%s/%s", entryFilename, upload.Filename)
					ur, err := upload.Open(h.Bucket)
					if err != nil {
						log.Print(err)
						return
					}
					defer ur.Close()

					if f, err := entryArchive.Create(filename); err != nil {
						log.Print(err)
						return
					} else if _, err := io.Copy(f, ur); err != nil {
						log.Print(err)
						return
					}
				}

				if err := entryArchive.Close(); err != nil {
					log.Print(err)
				}

				if f, err := playlistArchive.Create(entryFilename + ".zip"); err != nil {
					log.Print(err)
				} else if _, err := f.Write(entryBuffer.Bytes()); err != nil {
					log.Print(err)
				}
			}()
		}

		return false
	})
}

func (h *Handler) deletePlaylist(c *gin.Context) {
	result := h.DB.Delete(&models.Playlist{}, c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.Status(http.StatusNoContent)
	}
}

func (h *Handler) getPlaylistPreview(c *gin.Context) {
	playlist, slideNumber := h.getPlaylistAndSlideNumber(c)
	playlistPreview := models.PlaylistPreview{
		Playlist:    playlist,
		SlideNumber: slideNumber,
	}
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Preview Playlist",
		"Preview":     &playlistPreview,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/playlist-preview",
		HTMLData: &data,
		Data:     &playlistPreview,
	})
}

func (h *Handler) patchPlaylist(c *gin.Context, tx *gorm.DB) error {
	args := struct {
		Action models.PlaylistActionType `form:"action" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	var playlist *models.Playlist
	if err := tx.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry.Compo").
		First(&playlist, c.Param("playlist_id")).
		Error; err != nil {
		return err
	}

	redirectUrl := playlist.Path()

	action, err := playlist.FindAction(args.Action)
	if err != nil {
		return err
	}

	playlistShow, err := action(tx)
	if err != nil {
		return err
	}

	if args.Action == models.PlaylistActionShow {
		redirectUrl = playlistShow.Path()
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, redirectUrl)
	} else {
		c.JSON(http.StatusOK, playlistShow)
	}

	return nil
}

func (h *Handler) getPlaylistShow(c *gin.Context) {
	playlist, slideNumber := h.getPlaylistAndSlideNumber(c)
	playlistShow := models.PlaylistShow{
		Playlist:    playlist,
		SlideNumber: slideNumber,
	}
	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Show Playlist",
		"Show":        &playlistShow,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/playlist-show",
		HTMLData: &data,
		Data:     &playlistShow,
	})
}

func (h *Handler) postPlaylistShowSlide(c *gin.Context) {
	type PlaylistAction string
	const (
		Show PlaylistAction = "show"
	)
	args := struct {
		Action PlaylistAction `form:"action" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	if args.Action != Show {
		panic(fmt.Errorf("invalid action: %v", args.Action))
	}

	playlist, slideNumber := h.getPlaylistAndSlideNumber(c)
	playlistEntry := playlist.Entries[slideNumber-1]
	if err := playlistEntry.Show(h.DB); err != nil {
		panic(err)
	}

	playlistShow := models.PlaylistShow{
		Playlist:    playlist,
		SlideNumber: slideNumber,
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, playlistShow.Path())
	} else {
		c.JSON(http.StatusOK, &playlistShow)
	}
}

func (h *Handler) getPlaylistAndSlideNumber(c *gin.Context) (models.Playlist, int) {
	playlist := models.Playlist{}
	result := h.DB.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry.Uploads.Entry").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	uriArgs := struct {
		SlideNumber int `uri:"slide_number"`
	}{}
	if err := c.ShouldBindUri(&uriArgs); err != nil {
		panic(err)
	}

	slideNumber := max(min(uriArgs.SlideNumber, len(playlist.Entries)), 1)

	return playlist, slideNumber
}
