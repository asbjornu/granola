package admin

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"granola/models"
	"granola/problems"
	"granola/server/common"
)

func (h *Handler) getEntries(c *gin.Context) {
	var entries []models.Entry
	if err := h.DB.
		Preload("Uploads").
		Preload("Compo").
		Find(&entries).
		Error; err != nil {
		panic(problems.InternalServerError(err.Error()))
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Entries",
		"Entries":     entries,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/entries",
		HTMLData: &data,
		Data:     entries,
	})
}

func (h *Handler) getEntry(c *gin.Context, tx *gorm.DB) error {
	var entry *models.Entry
	if err := tx.
		Preload("Uploads").
		Preload("Uploads.Entry").
		Preload("Compo").
		First(&entry, "id = ?", c.Param("entry_id")).
		Error; err != nil {
		return err
	}

	var compos []models.Compo
	if err := tx.Find(&compos).Error; err != nil {
		return err
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Entry",
		"Entry":       &entry,
		"Compos":      compos,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  common.OfferedMIMETypes,
		HTMLName: "admin/entry",
		HTMLData: &data,
		Data:     &entry,
	})

	return nil
}

func (h *Handler) getEntryPreview(c *gin.Context) {
	entry := models.Entry{}
	result := h.DB.
		Preload("Compo").
		Preload("User").
		Preload("Uploads.Entry").
		First(&entry, "id = ?", c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Preview Entry",
		"Entry":       &entry,
		"EntryNumber": 0,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "slides/entry", &data)
}

func (h *Handler) getPlaylistEntry(c *gin.Context) {
	playlistEntry := models.PlaylistEntry{}
	result := h.DB.
		Preload("Playlist").
		Preload("Entry.Uploads").
		Where("playlist_id = ? and entry_id = ?",
			c.Param("playlist_id"),
			c.Param("entry_id")).
		First(&playlistEntry)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Preview Entry",
		"Entry":       &playlistEntry.Entry,
		"EntryNumber": playlistEntry.Order,
		"Playlist":    &playlistEntry.Playlist,
		"HttpContext": c.MustGet("HttpContext"),
	}
	c.HTML(http.StatusOK, "slides/entry", &data)
}

func (h *Handler) putEntry(c *gin.Context, tx *gorm.DB) error {
	args := struct {
		Title    string `json:"title" form:"title" binding:"required"`
		Author   string `json:"author" form:"author" binding:"required"`
		Platform string `json:"platform" form:"platform"`
		Notes    string `json:"notes" form:"notes"`
		CompoID  uint   `json:"compo" form:"compo" binding:"required"`
		Locked   *bool  `json:"locked" form:"locked" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		return err
	}

	var compo models.Compo
	if err := tx.First(&compo, args.CompoID).Error; err != nil {
		return problems.NotFound(fmt.Sprintf("compo %d not found", args.CompoID))
	}

	var entry *models.Entry
	if err := tx.
		Preload("Uploads").
		First(&entry, "id = ?", c.Param("entry_id")).
		Error; err != nil {
		return err
	}

	entry.Title = args.Title
	entry.Author = args.Author
	entry.Platform = args.Platform
	entry.Notes = args.Notes
	entry.Compo = compo
	entry.Locked = *args.Locked

	if err := tx.Save(&entry).Error; err != nil {
		return problems.InternalServerError(err.Error())
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, entry.Path())
	} else {
		c.JSON(http.StatusOK, &entry)
	}

	return nil
}

func (h *Handler) deleteEntry(c *gin.Context) {
	result := h.DB.Delete(&models.Entry{}, "id = ?", c.Param("entry_id"))

	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.Status(http.StatusNoContent)
	}
}
