package server

import (
	"github.com/gin-gonic/gin"
	"gocloud.dev/blob"
	"gorm.io/gorm"

	"granola/config"
	"granola/server/admin"
	"granola/server/common"
	"granola/server/user"
)

func NewUserServer(config *config.Config, db *gorm.DB, bucket *blob.Bucket) (*gin.Engine, user.Handler) {
	r, t := ConfigureGin(config)

	h := user.Handler{
		Translator: t,
		Config:     *config,
		DB:         db,
		Bucket:     bucket,
	}

	r.Use(gin.CustomRecoveryWithWriter(config.Logging.Writer, func(c *gin.Context, a any) {
		common.HandleError(c, t, a)
	}))
	r.NoRoute(common.Show404)

	r.Use(h.CheckAuth)
	h.RegisterRoutes(r)

	return r, h
}

func RunUserServer(config *config.Config, db *gorm.DB, bucket *blob.Bucket) {
	r, h := NewUserServer(config, db, bucket)

	h.PlaylistStreamer = user.NewPlaylistStreamer()
	go h.StreamEntries()

	if err := r.Run(":8080"); err != nil {
		panic(err)
	}
}

func NewAdminServer(config *config.Config, db *gorm.DB, bucket *blob.Bucket) (*gin.Engine, admin.Handler) {
	r, t := ConfigureGin(config)

	h := admin.Handler{
		Translator: t,
		Config:     *config,
		DB:         db,
		Bucket:     bucket,
	}

	r.Use(gin.CustomRecoveryWithWriter(config.Logging.Writer, func(c *gin.Context, a any) {
		common.HandleError(c, t, a)
	}))
	r.NoRoute(common.Show404)

	h.RegisterRoutes(r, config.Admins)

	return r, h
}

func RunAdminServer(config *config.Config, db *gorm.DB, bucket *blob.Bucket) {
	r, _ := NewAdminServer(config, db, bucket)
	if err := r.Run(":8008"); err != nil {
		panic(err)
	}
}
