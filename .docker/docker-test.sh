#!/bin/sh

DOCKER="${DOCKER:-docker}"

wait_for() {
    max_attempts=60
    attempt_num=1

    service=$1
    port_number=$2
    service_prefix=$(printf '%-12.12s' "${service}:${port_number}")

    echo "${service_prefix}: Waiting for service to start..."

    until ${DOCKER} logs "${service}" 2>&1 | grep -q "Listening and serving HTTP on :${port_number}"
    do
        if [ "${attempt_num}" -eq "${max_attempts}" ]; then
            echo "${service_prefix}: Attempt ${attempt_num} failed and there are no more attempts left!"
            ${DOCKER} logs "${service}"
            return 1
        else
            echo "${service_prefix}: Attempt ${attempt_num} failed! Trying again in 1 second..."
            sleep 1
        fi

        attempt_num=$((attempt_num+1))
    done
}

main() {
    pwd=$(pwd)

    ${DOCKER} compose -f "${pwd}/.docker/docker-compose.yml" up -d

    wait_for "granola" 8008 &
    admin_pid=$!

    wait_for "granola" 8080 &
    user_pid=$!

    wait "${admin_pid}" "${user_pid}" &&
    curl -f http://docker:8080

    exit_code=$?

    ${DOCKER} compose -f "${pwd}/.docker/docker-compose.yml" down

    return "${exit_code}"
}

main
