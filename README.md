# Granola

![Granola][logo]

A minimal, modern competition system for [demoparties].

## Design goals

- Minimal: This means no accounting system, no plugins. Just what you need
  to run competitions at parties.
- Portable: Moving the compo server to a different machine should be a
  matter of moving a folder of configuration and data, and restarting the
  application.
- *Small* data: No enterprise database. Even the biggest of demoparties are
  nowhere near needing big-data type databases. We're using SQLite, because
  that should be enough.
- Short lived: A demo party typically takes place over a weekend. There
  should be no need to migrate database layouts etc. So we don't even try
  to get this stuff right.
- Highly customizable: No hard-coded HTML anywhere. Everything should be
  rendered from templates, which is part of the configuration.
- Easily accessible data: All data stored in the system should be easily
  accessible using REST APIs.
- Secure: Both the admin interface and all API end-points should be
  authenticated when exposing non-public information.

## Getting Started

In order to build, you need the [Go][golang] programming language
installed.

It's fairly simple:

```kbd
$ go run . server
Listening on :8008...
Listening on :8080...
```

This brings up the user-facing interface on port 8080 and the admin-facing
interface on port 8008.

## Using Docker

By using Docker you do not need Go installed on your local machine, but
instead [Docker][docker]. This will download everything needed on first
run, but then be faster on restart.

```
$ cd .docker/
$ docker-compose up
Listening on :8008...
Listening on :8080...
```

## Icons

- [Cereal] by [Ayub Irawan][ayub] from [Noun Project][noun] (CC BY 3.0).
- All other icons are from [Ionicons] (MIT).

[ayub]: https://thenounproject.com/creator/ayub12/
[cereal]: https://thenounproject.com/icon/cereal-3989094/
[demoparties]: https://en.wikipedia.org/wiki/Demoscene#Parties
[docker]: https://docs.docker.com/get-docker/
[golang]: https://go.dev/dl/
[ionicons]: https://ionic.io/ionicons
[logo]: https://gitlab.com/granola-compo/granola/-/raw/main/static/logo/granola-black-outline.svg
[noun]: https://thenounproject.com/browse/icons/term/cereal/
