package config

type Config struct {
	Secret       []byte
	DatabasePath string
	BucketPath   string
	Admins       map[string]string
	Logging      LogConfig
	Routing      struct {
		BaseURL               string
		MinimumPasswordLength int
		MaxMultipartMemory    int64
		UseEntryMarkdown      bool
	}
	RateLimit struct {
		Register uint32
		Login    uint32
	}
}
