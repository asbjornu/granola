# Concepts

In Granola, the admins has to interact with some concepts, including:

- `Invite`: An invitation code to register a user.
- `Compo`: A competition that entries can be entered into.
- `Entry`: An entry into a compo.
- `Upload`: An uploaded file.
- `Playlist`: A playable/votable collection of entries. See below for
              distiontion from compo.

## Invites

An invite simply grants access to register a user in the compo system.
These should created before the party, and printed out and handed to
visitors.

This is traditionally called a "vote key", but we've decided to change the
name to make it a bit clearer, as they allow more than just voting.

## Compos vs Playlists

In Granola, as an admin, you create two related things: a compo and a
playlist. These sort of describe two different sides of the same concept.

A compo is a category to enter competition entries into. It's what users
see when they register entries. A compo can have a type (graphics, music
or real-time). They are typically created before the party starts, and do
not typically change during the party.

A playlist is a collection of competition entries, including an ordering.
They represent the displayable and votable side of a competition. A playlist
is created when a competition is prepared to be shown, and may contain
entries from multiple compos, in the case two competitions gets merged.

## Locking a Compo vs locking Entries

A compo can be locked once no more entries are accepted. Entries can still
be updated, unless they too have been locked. A compo will typically be locked
at convenient some point after the deadline has passed.

The idea is that once a compo organizer starts preparing for a compo, they
lock all entries. If there's an issue with an entry and the organizer allows
it to be fixed, the entry is unlocked, and submitter is notified. Once the
issue has been resolved, the entry should be locked again.

## Uploaded files

Granola allows an arbitrary amount of uploads per entry.

An upload can be the entry itself, or some meta-data. Here's some examples:
- A screen-shot of a demo
- A cover picture for a music entry
- A step-picture for a graphics entry
- A release-file to be published after the party

Granola generally tries to be free-form here, and allow a user to upload
what they think is relevant for the organizers.
