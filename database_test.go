package main

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"granola/config"
	"granola/data"
	"granola/models"
)

func TestInitDatabase(t *testing.T) {
	t.Run("empty init", func(t *testing.T) {
		db := data.OpenDatabase(":memory:", config.LogConfig{})
		data.InitDatabase(db, &data.InitialData{})

		var compos []models.Compo
		db.Find(&compos)

		assert.Empty(t, compos)

		var invites []models.Invite
		db.Find(&invites)

		assert.Empty(t, invites)
	})

	t.Run("some data", func(t *testing.T) {
		db := data.OpenDatabase(":memory:", config.LogConfig{})
		data.InitDatabase(db, &data.InitialData{
			Compos: []data.InitialDataCompo{
				{
					Name:          "Test Compo",
					Type:          models.CompoTypeGraphics,
					MultiPlatform: false,
					Description:   "Test Description",
				},
			},
			Invites: []string{
				"test-invite-1",
				"test-invite-2",
			},
		})

		var compos []models.Compo
		db.Find(&compos)

		assert.Equal(t, 1, len(compos))
		assert.Equal(t, "Test Compo", compos[0].Name)
		assert.Equal(t, models.CompoTypeGraphics, compos[0].Type)
		assert.Equal(t, false, compos[0].MultiPlatform)
		assert.Equal(t, "Test Description", compos[0].Description)

		var invites []models.Invite
		db.Find(&invites)

		assert.Equal(t, 2, len(invites))
		assert.Equal(t, "test-invite-1", invites[0].Key)
		assert.Equal(t, false, invites[0].UserID.Valid)
		assert.Equal(t, "test-invite-2", invites[1].Key)
		assert.Equal(t, false, invites[1].UserID.Valid)
	})

}
