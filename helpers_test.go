package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"granola/config"
	"granola/data"
	"granola/models"
	"granola/server"
	"granola/server/common"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gocloud.dev/blob"
	_ "gocloud.dev/blob/memblob"
	"gorm.io/gorm"
)

type Cleanupable interface {
	Cleanup(f func())
}

type TestRouter struct {
	Engine *gin.Engine
	DB     *gorm.DB
	Bucket *blob.Bucket
}

type TestResponseRecorder struct {
	*httptest.ResponseRecorder
	closeChannel chan bool
}

type TestResponse struct {
	ContentType string
	Location    string
	StatusCode  int
	Body        []byte
	Cookies     []string
}

func (response *TestResponse) BodyString() string {
	return string(response.Body)
}

func (response *TestResponse) BodyJSON() gin.H {
	if !strings.ContainsAny(response.ContentType, gin.MIMEJSON) && !strings.Contains(response.ContentType, "+json") {
		panic(fmt.Errorf("Content-Type is not JSON: %s", response.ContentType))
	}

	var h gin.H
	if err := json.Unmarshal(response.Body, &h); err != nil {
		s := response.BodyString()
		panic(fmt.Errorf("error converting JSON. %v %s", err, s))
	}

	return h
}

func (response *TestResponse) BodySlice() []any {
	if !strings.ContainsAny(response.ContentType, gin.MIMEJSON) && !strings.Contains(response.ContentType, "+json") {
		panic(fmt.Errorf("Content-Type is not JSON: %s", response.ContentType))
	}

	var slice []any
	if err := json.Unmarshal(response.Body, &slice); err != nil {
		s := response.BodyString()
		panic(fmt.Errorf("error converting JSON. %v %s", err, s))
	}

	return slice
}

func (r *TestResponseRecorder) CloseNotify() <-chan bool {
	return r.closeChannel
}

func (r *TestResponseRecorder) closeClient() {
	r.closeChannel <- true
}

func CreateTestResponseRecorder() *TestResponseRecorder {
	return &TestResponseRecorder{
		httptest.NewRecorder(),
		make(chan bool, 1),
	}
}

func (r *TestRouter) requestHTML(method string, path string, parameters gin.H, cookies []string) TestResponse {
	var body io.Reader = nil

	if len(parameters) > 0 {
		urlValues := url.Values{}
		for key, value := range parameters {
			urlValues.Add(key, fmt.Sprintf("%v", value))
		}
		body = strings.NewReader(urlValues.Encode())
	}

	req, _ := http.NewRequest(method, path, body)
	req.Header.Set("Accept", "text/html")

	if body != nil {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	for _, cookie := range cookies {
		req.Header.Set("Cookie", cookie)
	}

	w := httptest.NewRecorder()
	r.Engine.ServeHTTP(w, req)
	responseData, _ := io.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		Cookies:     w.Header().Values("Set-Cookie"),
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func (r *TestRouter) requestData(method string, path string, data []byte, filename string, cookies []string) TestResponse {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	contentType := writer.FormDataContentType()
	part, _ := writer.CreateFormFile("file", filename)
	if _, err := part.Write(data); err != nil {
		panic(err)
	}
	if err := writer.Close(); err != nil {
		panic(err)
	}
	return r.request(method, path, contentType, body, cookies)
}

func (r *TestRouter) requestJson(method string, path string, obj gin.H, cookies []string) TestResponse {
	d, _ := json.Marshal(obj)
	body := bytes.NewBuffer(d)
	contentType := fmt.Sprintf("%s; charset=UTF-8", gin.MIMEJSON)
	return r.request(method, path, contentType, body, cookies)
}

func (r *TestRouter) request(method string, path string, contentType string, body io.Reader, cookies []string) TestResponse {
	req, _ := http.NewRequest(method, path, body)
	req.Header.Set("Content-Type", contentType)
	req.Header.Set("Accept", fmt.Sprintf("%s, %s", gin.MIMEJSON, common.MIMEProblemJSON))

	for _, cookie := range cookies {
		req.Header.Set("Cookie", cookie)
	}

	w := CreateTestResponseRecorder()
	r.Engine.ServeHTTP(w, req)

	responseData, _ := io.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		Cookies:     w.Header().Values("Set-Cookie"),
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func DefaultTestConfig() config.Config {
	cfg := config.Config{
		Secret:       []byte("not-very-secret"),
		DatabasePath: ":memory:",
		Admins:       map[string]string{"admin": "password"},
	}

	cfg.Routing.BaseURL = "http://test-host"
	cfg.Routing.MinimumPasswordLength = 5
	cfg.Routing.UseEntryMarkdown = true
	cfg.RateLimit.Register = ^uint32(0)
	cfg.RateLimit.Login = ^uint32(0)

	if !testing.Verbose() {
		cfg.Logging.Writer = io.Discard
		cfg.Logging.Level = config.Silent
	}

	return cfg
}

func openTestDatabase(t Cleanupable, config *config.Config) *gorm.DB {
	db := data.OpenDatabase(config.DatabasePath, config.Logging)

	t.Cleanup(func() {
		sql, _ := db.DB()
		if err := sql.Close(); err != nil {
			log.Print(err)
		}
		db = nil
	})

	return db
}

func openTestBucket(t Cleanupable, config *config.Config) *blob.Bucket {
	bucket := data.OpenBucket(fmt.Sprintf("mem://%s", config.BucketPath))

	t.Cleanup(func() {
		if err := bucket.Close(); err != nil {
			log.Print(err)
		}
		bucket = nil
	})

	return bucket
}

func NewTestRouterUserWithConfig(t Cleanupable, c *config.Config) TestRouter {
	uuid.SetRand(rand.New(rand.NewSource(1)))
	gin.SetMode(gin.TestMode)

	db := openTestDatabase(t, c)
	bucket := openTestBucket(t, c)

	r, _ := server.NewUserServer(c, db, bucket)
	return TestRouter{
		Engine: r,
		DB:     db,
		Bucket: bucket,
	}
}

func NewTestRouterUser(t Cleanupable) TestRouter {
	c := DefaultTestConfig()
	return NewTestRouterUserWithConfig(t, &c)
}

func NewTestRouterAdminWithConfig(t Cleanupable, c *config.Config) TestRouter {
	uuid.SetRand(rand.New(rand.NewSource(1)))
	gin.SetMode(gin.TestMode)

	db := openTestDatabase(t, c)
	bucket := openTestBucket(t, c)

	r, _ := server.NewAdminServer(c, db, bucket)
	return TestRouter{
		Engine: r,
		DB:     db,
		Bucket: bucket,
	}
}

func NewTestRouterAdmin(t Cleanupable) TestRouter {
	c := DefaultTestConfig()
	return NewTestRouterAdminWithConfig(t, &c)
}

func (r *TestRouter) createUser(email string, handle string, password string, invite string) *models.User {
	user, err := models.NewUserWithInvite(r.DB, email, handle, password, invite)
	if err != nil {
		panic(err)
	}

	return user
}

func (r *TestRouter) loginUser(email string, password string) []string {
	obj := gin.H{
		"email":    email,
		"password": password,
	}
	response := r.requestJson("POST", "/login", obj, nil)
	if response.StatusCode != http.StatusOK {
		panic("failed to log in")
	}
	return response.Cookies
}

func (r *TestRouter) adminRequest(method string, path string, obj gin.H) TestResponse {
	d, _ := json.Marshal(obj)
	body := bytes.NewBuffer(d)
	req, _ := http.NewRequest(method, path, body)
	req.SetBasicAuth("admin", "password")
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	w := CreateTestResponseRecorder()
	r.Engine.ServeHTTP(w, req)

	responseData, _ := io.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func (r *TestRouter) adminRequestHTML(method string, path string) TestResponse {
	req, _ := http.NewRequest(method, path, nil)
	req.SetBasicAuth("admin", "password")
	req.Header.Set("Accept", "text/html")
	w := httptest.NewRecorder()
	r.Engine.ServeHTTP(w, req)

	responseData, _ := io.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func (r *TestRouter) adminPostFormData(path string, data map[string]string) TestResponse {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	for k, v := range data {
		part, err := writer.CreateFormField(k)
		if err != nil {
			panic(err)
		}
		part.Write([]byte(v))
	}
	writer.Close()

	req, _ := http.NewRequest("POST", path, body)
	req.SetBasicAuth("admin", "password")
	req.Header.Set("Content-Type", writer.FormDataContentType())
	w := CreateTestResponseRecorder()
	r.Engine.ServeHTTP(w, req)

	responseData, _ := io.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func (r *TestRouter) createCompo(name string, multiPlatform bool, locked bool, ct models.CompoType, description string) *models.Compo {
	compo, err := models.NewCompo(r.DB, name, multiPlatform, locked, ct, description)
	if err != nil {
		panic(err)
	}
	return compo
}

func (r *TestRouter) createTestCompo() *models.Compo {
	return r.createCompo("test compo", true, false, models.CompoTypeUndefined, "test description")
}

func (r *TestRouter) createInvite(key string, canVote bool) {
	_, err := models.NewInvite(r.DB, key, &models.UserPermissions{CanVote: canVote})
	if err != nil {
		panic(err)
	}
}

func (r *TestRouter) createTestUser() *models.User {
	r.createInvite("test-invite", false)
	return r.createUser("test-user@example.com", "test-user", "test-password", "test-invite")
}

func (r *TestRouter) lockEntry(entry *models.Entry) {
	entry.Locked = true

	result := r.DB.Save(&entry)
	if result.Error != nil {
		panic(result.Error)
	}
}

func (r *TestRouter) createUserEntry(title string, author string, platform string, notes string, compo *models.Compo, user *models.User) *models.Entry {
	entry, err := models.NewEntry(r.DB, title, author, platform, notes, compo, user, false)
	if err != nil {
		panic(err)
	}
	return entry
}

func (r *TestRouter) createEntry(compo *models.Compo, title string, author string, platform string, notes string) *models.Entry {
	return r.createUserEntry(title, author, platform, notes, compo, &models.User{})
}

func (r *TestRouter) createUserTestEntry(compo *models.Compo, user *models.User) *models.Entry {
	return r.createUserEntry("test-entry", "test-author", "test-platform", "test-notes", compo, user)
}

func (r *TestRouter) createTestEntry(compo *models.Compo) *models.Entry {
	return r.createEntry(compo, "test-entry", "test-author", "test-platform", "test-notes")
}

func (r *TestRouter) createUpload(entry *models.Entry, filename string, data []byte) *models.Upload {
	upload, err := models.NewUploadFromData(r.DB, r.Bucket, filename, data, entry)
	if err != nil {
		panic(err)
	}
	return upload
}

func (r *TestRouter) createPlaylist(name string, entries []uuid.UUID) *models.Playlist {
	playlist, err := models.NewPlaylist(r.DB, name, entries)
	if err != nil {
		panic(err)
	}

	return playlist
}

func (r *TestRouter) createVote(db *gorm.DB, entry *models.Entry, user *models.User, score uint) *models.Vote {
	vote, err := models.NewVote(r.DB, entry, user, score)
	if err != nil {
		panic(err)
	}

	return vote
}

func (r *TestRouter) createVotes() {
	err := r.DB.Transaction(func(tx *gorm.DB) error {
		for i := 1; i <= 5; i++ {
			compo, err := models.NewCompo(tx,
				fmt.Sprintf("compo-%d", i),
				false,
				false,
				models.CompoTypeUndefined,
				"")
			if err != nil {
				return err
			}

			password := fmt.Sprintf("password-%d", i)
			user, err := models.NewUser(tx,
				fmt.Sprintf("user-%d@example.com", i),
				fmt.Sprintf("handle-%d", i),
				&password,
				&models.UserPermissions{CanVote: true})
			if err != nil {
				return err
			}

			playlist := &models.Playlist{Name: fmt.Sprintf("playlist-%d", i)}
			if err = tx.Save(playlist).Error; err != nil {
				return err
			}

			for j := 1; j <= 5; j++ {
				entry, err := models.NewEntry(tx,
					fmt.Sprintf("title-%d", i),
					user.Handle,
					fmt.Sprintf("platform-%d", i),
					fmt.Sprintf("notes-%d", i),
					compo,
					user, false)

				if err != nil {
					return err
				}

				playlistEntry := &models.PlaylistEntry{
					PlaylistID: playlist.ID,
					EntryID:    entry.ID,
				}
				if err = tx.Save(playlistEntry).Error; err != nil {
					return err
				}

				for k := 1; k <= j; k++ {
					score := uint(k)
					if i%2 == 0 {
						// Every other user should give a bad score as its latest vote
						score = 5 - uint(k) + 1
					}
					_, err := models.NewVote(tx, entry, user, score)
					if err != nil {
						return err
					}
				}
			}
		}

		return nil
	})
	if err != nil {
		panic(err)
	}
}

func (r *TestRouter) createMultiplePlaylists(db *gorm.DB, compos int, entriesPerCompo int) []*models.Playlist {
	var playlists []*models.Playlist
	for i := 1; i <= compos; i++ {
		compo, err := models.NewCompo(db,
			fmt.Sprintf("compo-%d", i),
			false,
			false,
			models.CompoTypeUndefined,
			"")
		if err != nil {
			panic(err)
		}

		playlist := &models.Playlist{Name: fmt.Sprintf("playlist-%d", i)}
		if err = db.Save(playlist).Error; err != nil {
			panic(err)
		}

		for j := 1; j <= entriesPerCompo; j++ {
			entry, err := models.NewEntry(db,
				fmt.Sprintf("title-%d", i),
				fmt.Sprintf("author-%d", i),
				fmt.Sprintf("platform-%d", i),
				fmt.Sprintf("notes-%d", i),
				compo,
				&models.User{}, false)

			if err != nil {
				panic(err)
			}

			playlistEntry := &models.PlaylistEntry{
				PlaylistID: playlist.ID,
				EntryID:    entry.ID,
				Entry:      *entry,
			}
			if err = db.Save(playlistEntry).Error; err != nil {
				panic(err)
			}
			playlist.Entries = append(playlist.Entries, *playlistEntry)
		}
		playlists = append(playlists, playlist)
	}
	return playlists
}

func (r *TestRouter) createMultipleVotingUsers(db *gorm.DB, numUsers int) []*models.User {
	var users []*models.User
	for i := 1; i <= numUsers; i++ {

		password := fmt.Sprintf("password-%d", i)
		user, err := models.NewUser(db,
			fmt.Sprintf("voting-user-%d@example.com", i),
			fmt.Sprintf("voting-handle-%d", i),
			&password,
			&models.UserPermissions{CanVote: true})
		if err != nil {
			panic(err)
		}
		users = append(users, user)
	}
	return users
}

func (r *TestRouter) createRandomVotes(playlists []*models.Playlist, users []*models.User) {
	err := r.DB.Transaction(func(tx *gorm.DB) error {
		// cast votes
		for _, playlist := range playlists {
			for _, user := range users {
				for _, entry := range playlist.Entries {
					score := uint(rand.Intn(5))
					_, err := models.NewVote(tx, &entry.Entry, user, score)
					if err != nil {
						return err
					}
				}
			}
		}

		return nil
	})
	if err != nil {
		panic(err)
	}
}
