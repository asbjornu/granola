(function() {
  class Voting {
    votingContainer;

    constructor(votingContainer) {
      if (votingContainer === null || votingContainer === undefined) {
        throw 'Voting container is required';
      }

      this.votingContainer = votingContainer;
    }

    cloneTemplate() {
      if ('content' in document.createElement('template')) {
        const template = document.getElementById('voting-template');
        return template.content.firstElementChild.cloneNode(true);
      }

      return null;
    }

    initializeEventSourceStream() {
      const self = this;

      if (!document.getElementById('voting').classList.contains('live')) {
        // The event source is only needed for live-voting
        return null;
      }

      const stream = new EventSource('/voting/live/stream');
      window.addEventListener('beforeunload', function() {
        self.clearEventSourceStreamStatus();
      });
      stream.addEventListener('error', function(e) {
        stream.close();
        console.error(e);

        setTimeout(function() {
          self.connectEventSourceStream();
        }, 1000);
      });

      var lastVotingEntry = null;
      stream.addEventListener('message', function(e) {
        if (!e.data) return;

        const votingEntry = JSON.parse(e.data)

        if (!votingEntry) {
          return;
        }

        if (lastVotingEntry != null
            && votingEntry.id === lastVotingEntry.id
            && votingEntry.playlist.liveVotingClosedAt === lastVotingEntry.playlist.liveVotingClosedAt) {
          return;
        }

        if (votingEntry.playlist.liveVotingClosedAt) {
          location.reload();
        } else {
          self.handleReceivedVotingEntry(votingEntry);
          lastVotingEntry = votingEntry;
        }
      });

      return stream;
    }

    handleReceivedVotingEntry(votingEntry) {
      const existingEntrySelector = this.votingContainer.querySelector(`.entry[data-entry-id="${votingEntry.id}"]`)
      if (existingEntrySelector) {
        return;
      }

      const clone = this.cloneTemplate();
      if (!clone) {
        console.error('Your browser does not support templates');
        this.votingContainer.innerHTML = 'Your browser does not support templates';
        return;
      }

      this.refreshEntry(clone, votingEntry);
      const entries = this.votingContainer.querySelector('.entries');
      entries.prepend(clone);
    }

    refreshEntry(entry, votingEntry) {
      entry.querySelectorAll('.place-vote form').forEach(function(form) {
        form.setAttribute('action', votingEntry.votes.id)
      });

      entry.setAttribute('class', 'entry')
      entry.setAttribute('data-entry-id', votingEntry.id);
      entry.setAttribute('id', `entry-${votingEntry.order}`);
      entry.querySelector('iframe').setAttribute('src',  votingEntry.id);

      if (votingEntry.vote && votingEntry.vote.score > 0) {
        this.refreshVote(entry, votingEntry.vote);
      }

      entry.querySelector('label.entry-frame').setAttribute('for', `entry-${votingEntry.order}`);
    }

    refreshVote(currentVoteContainer, vote) {
      const voteText = currentVoteContainer.querySelector('.vote-text');
      voteText.classList.remove('hidden');
      const time = voteText.querySelector('.time');
      time.setAttribute('datetime', vote.created.dateTime);
      time.innerHTML = vote.created.humanized;
      voteText.querySelector('.score').innerText = vote.score;

      currentVoteContainer.querySelectorAll('.place-vote .star').forEach(function(star) {
        star.classList.remove('selected');
      });

      currentVoteContainer.querySelector(`.place-vote .star-${vote.score}`).classList.add('selected');
    }

    clearEventSourceStreamStatus() {
      if (this.interval != undefined) {
        clearInterval(this.interval);
        this.interval = undefined;
      }
    }

    setUpEventSourceStreamStatus(stream) {
      if (stream === null) {
        return;
      }

      const status = document.querySelector('#voting .status');
      const disconnectedDialog = document.querySelector('#disconnected-dialog');
      const updateStatus = function() {
        const state = (function() {
          switch (stream.readyState) {
            case EventSource.CONNECTING:
              return 'Connecting...';
            case EventSource.OPEN:
              return 'Connected!';
            case EventSource.CLOSED:
              return 'Reconnecting...';
          }

          return 'Unknown';
        })();

        if (stream.readyState == EventSource.OPEN)
          disconnectedDialog.close();
        else if (stream.readyState == EventSource.CLOSED)
          disconnectedDialog.show();

        status.innerHTML = state;
      };

      this.interval = setInterval(updateStatus, 100);
    }

    setUpVoteSubmission() {
      var self = this;
      this.votingContainer.querySelectorAll('.place-vote form').forEach(function(form) {
        form.addEventListener('submit', function(e) {
          e.preventDefault();

          const score = parseInt(this.querySelector('button[name="score"]').value, 10);

          let options = {
            method: this.getAttribute('method'),
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ score: score })
          }

          fetch(this.getAttribute('action'), options)
            .then(function(response) {
              return response.json()
            })
            .then(function(vote) {
              let parentNode = function(parent) {
                while (parent = parent.parentNode) {
                  if (parent.classList && parent.classList.contains('vote')) {
                    return parent;
                  }
                }

                return null;
              }(form);

              self.refreshVote(parentNode, vote);
            }).catch(function(error) {
              console.error(error);
            });

          return false;
        });
      });
    }

    connectEventSourceStream() {
      var self = this;
      const stream = this.initializeEventSourceStream();
      this.clearEventSourceStreamStatus();
      setTimeout(function() {
        self.setUpEventSourceStreamStatus(stream);
      }, 1000);
    }
  }


  document.addEventListener('DOMContentLoaded', function() {
    const votingContainer = document.querySelector('#voting .voting-container');
    if (votingContainer) {
      const voting = new Voting(votingContainer)
      voting.connectEventSourceStream();
      voting.setUpVoteSubmission();
    }
  });
})();
