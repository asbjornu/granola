package data

import (
	"context"

	"gocloud.dev/blob"
)

func OpenBucket(path string) *blob.Bucket {
	bucket, err := blob.OpenBucket(context.Background(), path)
	if err != nil {
		panic(err)
	}
	return bucket
}
