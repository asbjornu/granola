package main

import (
	"crypto/rand"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"granola/config"
	"granola/data"
	"granola/server"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
	_ "gocloud.dev/blob/fileblob"
	"gopkg.in/yaml.v3"
)

func main() {
	viper.SetDefault("routing.BaseURL", "http://localhost:8080")
	viper.SetDefault("routing.MinimumPasswordLength", 5)
	viper.SetDefault("routing.MaxMultipartMemory", 128<<20) // 128 MiB
	viper.SetDefault("routing.UseEntryMarkdown", true)
	viper.SetDefault("logging.Level", config.Info)
	viper.SetDefault("DatabasePath", "db/database.db")
	viper.SetDefault("BucketPath", "db/blobs")
	viper.SetDefault("rateLimit.register", 1)
	viper.SetDefault("rateLimit.login", 10)
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal(err)
	}

	c := config.Config{Secret: secret()}

	if err := viper.Unmarshal(&c, viper.DecodeHook(mapstructure.TextUnmarshallerHookFunc())); err != nil {
		log.Fatal("Failed to unmarshal configuration", err)
	}

	if c.Logging.Writer == nil {
		c.Logging.Writer = log.Writer()
	} else {
		log.SetOutput(c.Logging.Writer)
	}

	if err := os.MkdirAll(filepath.Dir(c.DatabasePath), os.ModePerm); err != nil {
		log.Fatal("Failed to create leading directories")
	}

	if err := os.MkdirAll(c.BucketPath, os.ModePerm); err != nil {
		log.Fatal("Failed to create leading directories")
	}

	if len(os.Args) < 2 {
		log.Fatal("Please specify a subcommand.")
	}

	cmd := os.Args[1]
	switch cmd {
	case "server":
		db := data.OpenDatabase(c.DatabasePath, c.Logging)
		bucket := data.OpenBucket(fmt.Sprintf("file://./%s?no_tmp_dir=1", c.BucketPath))
		go server.RunAdminServer(&c, db, bucket)
		server.RunUserServer(&c, db, bucket)
	case "init":
		if len(os.Args) < 3 {
			log.Fatal("Please specify a YML file.")
		}

		if _, err := os.Stat(c.DatabasePath); err == nil {
			log.Fatalf("Database file ('%s') already exists!", c.DatabasePath)
		}

		db := data.OpenDatabase(c.DatabasePath, c.Logging)
		yamlFile, err := os.ReadFile(os.Args[2])
		if err != nil {
			panic(err)
		}

		var initialData data.InitialData
		err = yaml.Unmarshal(yamlFile, &initialData)
		if err != nil {
			panic(err)
		}

		data.InitDatabase(db, &initialData)
	default:
		log.Fatalf("Unrecognized command %q. "+
			"Command must be one of: init, server", cmd)
	}
}

func secret() []byte {
	var secretBytes []byte
	if !viper.IsSet("cookie.secret") {
		log.Print("WARNING: cookie.secret not set, using random bytes. This means cookies won't persist between runs")
		secretBytes = make([]byte, 512)
		_, err := rand.Read(secretBytes)
		if err != nil {
			panic(err)
		}
	} else {
		secret := viper.GetString("cookie.secret")
		secretBytes = []byte(secret)
	}

	return secretBytes
}
